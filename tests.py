from app import app
import unittest
from flask import json
from common.globals import RESULTS


class TestsPageTestCase(unittest.TestCase):
    """Checking the return of a valid html page for testing the quality of communication"""
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)


class MainParamsTestCase(unittest.TestCase):
    """Checking that the correct data is returned for the test object"""
    def test_current_json_response_for_main_params_get_request(self):
        RESULTS.update({'name': 'Red Square', 'point_coords': (55.753929, 37.620274)})

        tester = app.test_client(self)
        response = tester.get('/api/v1.0/point')

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'point_name': 'Red Square',
                'point_lat': 55.753929,
                'point_lon': 37.620274
            }
        )

    def test_current_json_response_for_main_params_post_request(self):
        tester = app.test_client(self)
        response = tester.post(
            '/api/v1.0/point',
            content_type='application/json',
            data=json.dumps(dict(point_name='Red Square', point_lat=55.753929, point_lon=37.620274))
        )

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'message': 'Point saved!',
                'point_name': 'Red Square',
                'point_lat': 55.753929,
                'point_lon': 37.620274
            }
        )


class ServerParamsTestCase(unittest.TestCase):
    """Verifying that correct data is returned for the test server"""
    def test_current_json_response_for_server_params_get_request(self):
        tester = app.test_client(self)
        response = tester.get('/api/v1.0/server')

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'server': RESULTS['server'],
                'n_packets': RESULTS['n_packets'],
                'packet_size': RESULTS['packet_size'],
                'bandwidth': RESULTS['bandwidth'],
                'duration': RESULTS['duration'],
            }
        )

    def test_current_json_response_for_server_params_post_request(self):
        tester = app.test_client(self)
        response = tester.post(
            '/api/v1.0/server',
            content_type='application/json',
            data=json.dumps(
                dict(
                    server='10.10.10.1',
                    n_packets=512,
                    packet_size=64,
                    duration=200,
                    bandwidth=64
                )
            )
        )

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'server': '10.10.10.1',
                'n_packets': 512,
                'packet_size': 64,
                'duration': 200,
                'bandwidth': 64000,
                'message': 'Server saved!'
            }
        )


class FresnelZoneTestCase(unittest.TestCase):
    def test_current_json_response_for_fresnel_zone_get_request(self):
        RESULTS.update(
            {
                'fresnel_image': 'test',
                'bs_height': 75,
                'ant_height': 3.5,
                'freq': 900,
                'dist': 12.5,
                'fresnel_60_top_overlap': True,
                'fresnel_60_bottom_overlap': True,
                'direct_line_overlap': True
            }
        )

        tester = app.test_client(self)
        response = tester.get('/api/v1.0/fresnel_zone')
        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'fresnel_image': 'test',
                'bs_height': 75,
                'point_ant_height': 3.5,
                'freq': 900,
                'dist': 12.5,
                'fresnel_60_top_overlap': True,
                'fresnel_60_bottom_overlap': True,
                'direct_line_overlap': True
            }
        )

    def test_current_json_response_for_fresnel_zone_post_request(self):
        tester = app.test_client(self)

        test_data = {
            'point_name': 'Test',
            'point_lat': 53.2,
            'point_lon': 33.3,
            'bs_lat': 54.3,
            'bs_lon': 34.5,
            'freq': 900,
            'bs_height': 75,
            'ant_height': 3.5,
        }

        response = tester.post(
            '/api/v1.0/fresnel_zone',
            content_type='application/json',
            data=json.dumps(test_data)
        )

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertIn('fresnel_image', data)
        self.assertIn('dist', data)
        self.assertIn('fresnel_60_top_overlap', data)
        self.assertIn('fresnel_60_bottom_overlap', data)
        self.assertIn('direct_line_overlap', data)
        self.assertEqual('Fresnel zone drawn!', data['message'])


class CellSignalTestCase(unittest.TestCase):
    def test_current_json_response_for_signal_get_request(self):
        tester = app.test_client(self)
        response = tester.get('/api/v1.0/signal')

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertIn('asu', data)
        self.assertIn('cellid', data)
        self.assertIn('error', data)
        self.assertIn('freq', data)
        self.assertIn('net_type', data)
        self.assertIn('net_mode', data)
        self.assertIn('operator_code', data)
        self.assertIn('operator_name', data)
        self.assertIn('rssi', data)


class BandwidthTestTestCase(unittest.TestCase):
    def test_current_json_response_for_bandwidth_test_get_request(self):
        RESULTS.update(
            {
                'download': 11765.0,
                'download_data': [
                    7260.0,
                    14200.0,
                    14700.0,
                    9700.0,
                    8690.0,
                    12100.0,
                    12900.0,
                    13100.0,
                    11300.0,
                    13700.0
                ],
                'speed_result_str': 'test_result',
                'upload': 1537.0,
                'upload_data': [
                    11300.0,
                    4070.0000000000005,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0
                ]
            }
        )

        tester = app.test_client(self)
        response = tester.get('/api/v1.0/speed')
        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'download': 11765.0,
                'download_data': [
                    7260.0,
                    14200.0,
                    14700.0,
                    9700.0,
                    8690.0,
                    12100.0,
                    12900.0,
                    13100.0,
                    11300.0,
                    13700.0
                ],
                'speed_result_str': 'test_result',
                'upload': 1537.0,
                'upload_data': [
                    11300.0,
                    4070.0000000000005,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0
                ]
            }
        )

    def test_current_json_response_for_bandwidth_test_post_request(self):
        tester = app.test_client(self)

        response = tester.post(
            '/api/v1.0/speed',
            content_type='application/json',
            data=json.dumps(dict(server='185.80.129.124', bandwidth=64000000, duration=5))
        )
        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertIn('download', data)
        self.assertIn('upload', data)
        self.assertIn('download_data', data)
        self.assertIn('speed_result_str', data)
        self.assertIn('upload_data', data)
        self.assertEqual('Test complete!', data['message'])


class PingTestTestCase(unittest.TestCase):
    def test_current_json_response_for_ping_test_get_request(self):
        RESULTS.update(
            {
                'count_max': 0,
                'count_max_percent': 0.0,
                'max_ping': 68.992,
                'mean_ping': 61.939,
                'min_ping': 48.099,
                'p_loss': 0.0,
                'ping_raw_results': 'test_string'
            }
        )

        tester = app.test_client(self)
        response = tester.get('/api/v1.0/ping')
        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            data,
            {
                'count_max': 0,
                'count_max_percent': 0.0,
                'max_ping': 68.992,
                'mean_ping': 61.939,
                'min_ping': 48.099,
                'p_loss': 0.0,
                'ping_raw_results': 'test_string'
            }
        )

    def test_current_json_response_for_ping_test_post_request(self):
        tester = app.test_client(self)

        response = tester.post(
            '/api/v1.0/ping',
            content_type='application/json',
            data=json.dumps(dict(server='185.80.129.124', n_packets=5, packet_size=64))
        )

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(response.status_code, 200)
        self.assertIn('min_ping', data)
        self.assertIn('mean_ping', data)
        self.assertIn('max_ping', data)
        self.assertIn('count_max', data)
        self.assertIn('p_loss', data)
        self.assertIn('count_max_percent', data)
        self.assertIn('ping_raw_results', data)
        self.assertEqual('Test complete!', data['message'])


if __name__ == '__main__':
    unittest.main()
