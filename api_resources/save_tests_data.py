"""
API for saving test results to a database
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from common.globals import RESULTS
from common.data_storage import save_to_database, save_to_json


class Storage(Resource):
    """Resource for setting test server parameters"""
    def get(self):
        pass

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('database', type=bool, help='Use database or not')
        request_args = parser.parse_args()

        db = request_args.get('database')
        if db:
            save_to_database(RESULTS)
            message = 'Saved to database!'
        else:
            save_to_json(RESULTS)
            message = 'Saved to JSON, path: ...!'

        return jsonify(
            message=message
        )
