"""
API for ping test
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from common.globals import RESULTS, save_results
from common.ping_test import measure_ping


class PingTest(Resource):
    """Resource for setting test server parameters"""
    def get(self):
        return jsonify(
            max_ping=RESULTS.get('max_ping'),
            min_ping=RESULTS.get('min_ping'),
            mean_ping=RESULTS.get('mean_ping'),
            p_loss=RESULTS.get('p_loss'),
            count_max=RESULTS.get('count_max'),
            count_max_percent=RESULTS.get('count_max_percent'),
            ping_raw_results=RESULTS.get('ping_raw_results'),
        )

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('server', type=str, help='Address of the testing server')
        parser.add_argument('n_packets', type=int, help='Number of ping-test packets')
        parser.add_argument('packet_size', type=int, help='Number of data bytes to be sent')
        request_args = parser.parse_args()

        ping_test_results = measure_ping(
            request_args.get('server'),
            str(request_args.get('n_packets')),
            str(request_args.get('packet_size'))
        )

        save_results(RESULTS, **ping_test_results)

        return jsonify(
            max_ping=RESULTS.get('max_ping'),
            min_ping=RESULTS.get('min_ping'),
            mean_ping=RESULTS.get('mean_ping'),
            p_loss=RESULTS.get('p_loss'),
            count_max=RESULTS.get('count_max'),
            count_max_percent=RESULTS.get('count_max_percent'),
            ping_raw_results=RESULTS.get('ping_raw_results'),
            ping_list=RESULTS.get('ping_list'),
            message='Test complete!'
        )
