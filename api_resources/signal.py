"""
Cell Signal params API
"""
from flask import jsonify
from flask_restful import Resource
from requests.exceptions import ConnectionError
from common.globals import RESULTS, save_results
from common.modems.modem_controller import modem


class SignalDetailed(Resource):
    """Resource for receiving the parameters of a cellular network signal"""
    def get(self):
        rssi = None
        asu = 0
        operator_name = None
        operator_code = None
        cellid = None
        freq = 900
        net_type = None
        error = None
        try:
            rssi, cellid, freq, net_type, operator_name, operator_code, asu = modem.measure_signal_params()
        except (ConnectionError, AttributeError):
            error = 'No connection to modem! Please check device performance.'
        save_results(
            RESULTS,
            rssi=rssi,
            asu=asu,
            operator_name=operator_name,
            operator_code=operator_code,
            cellid=cellid,
            freq=freq,
            net_type=net_type,
            modem_type=str(modem),
            error=error
        )

        return jsonify(
            rssi=rssi,
            asu=asu,
            freq=freq,
            cellid=cellid,
            net_type=net_type,
            operator_name=operator_name,
            operator_code=operator_code,
            modem_type=str(modem),
            error=error
        )


class Signal(Resource):
    """Resource for receiving the simplified signal information"""

    def get(self):
        rssi = None
        net_type = None
        operator_name = None
        error = None
        try:
            rssi = modem.get_rssi()
            net_type = modem.get_current_net_type()
            operator_name, _operator_code = modem.get_current_mobile_operator()
            save_results(RESULTS, rssi=rssi, signal_test=True)
        except (ConnectionError, AttributeError):
            error = 'No connection to modem! Please check device performance.'
        return jsonify(
            rssi=rssi,
            net_type=net_type,
            operator_name=operator_name,
            error=error
        )
