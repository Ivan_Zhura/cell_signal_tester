import os
import sqlite3
from flask_restful import Resource
from flask import jsonify
from flask_restful import reqparse
from requests.exceptions import ConnectionError as ModemConnectionError
from common.globals import RESULTS, save_results
from common.modems.modem_controller import modem

# For testing:
# /bs_list?operator_code=25099&cellid=28065&net_type=2G
# get('http://localhost:5000/bs_list', data=dict(operator_code=25099, cellid=28065, net_type='2G')).json()
# post('http://localhost:5000/bs_list', data={'lac': 16301, 'bs_lat': 53.26815, 'bs_lon': 50.259424}).json()
# cellid = 28065
# lac = '16301'
# net_type = '2G'
# operator_code = '25099'


class BaseStationList(Resource):
    """Resource for determining the coordinates of base stations"""
    def get(self):
        operator_code = None
        cellid = None
        lac = None
        net_type = None
        error = None
        try:
            cellid, lac, net_type, operator_code = modem.measure_bs_params()
            save_results(RESULTS, lac=lac)
        except (ModemConnectionError, AttributeError):
            error = 'No connection to modem! Please check device performance.'

        try:
            mcc = operator_code[:3]
            mnc = operator_code[3:]
        except TypeError:
            mcc = None
            mnc = None

        network_types_dict = {
            '2G': ('GSM'),
            '3G': ('WCDM', 'UMTS', 'CDMA'),
            '4G': ('LTE')
        }

        db_connection = sqlite3.connect(os.path.join('common', 'data', 'russian_cells.db'))
        cursor = db_connection.cursor()
        question_marks = '?' * len(network_types_dict.get(net_type))
        sql = f"""SELECT lat, lon  
FROM cells 
WHERE cellid=? AND mcc=? AND mnc=? AND lac=? AND radio_type IN ({','.join(question_marks)})"""
        params = [cellid, mcc, mnc, lac]
        params.extend(network_types_dict.get(net_type))
        cursor.execute(sql, params)
        bs_list = cursor.fetchall()

        return jsonify(
            mcc=mcc,
            mnc=mnc,
            cellid=cellid,
            lac=lac,
            net_type=net_type,
            bs_list=bs_list,
            error=error
        )

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('bs_lat', type=float, help='Base station latitude')
        parser.add_argument('bs_lon', type=float, help='Base station longitude')
        request_args = parser.parse_args()
        bs_lat = request_args.get('bs_lat')
        bs_lon = request_args.get('bs_lon')
        bs_coords = (bs_lat, bs_lon)

        save_results(RESULTS, bs_coords=bs_coords)
        return jsonify(bs_coords=bs_coords)


class BaseStationCoordinates(Resource):
    """Resource for getting saved coordinates of base stations from the server"""
    def get(self):
        return jsonify(
            bs_coords=RESULTS.get('bs_coords')
        )
