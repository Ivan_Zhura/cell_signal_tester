"""
API for testing bandwidth
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from common.globals import RESULTS, save_results
from common.speed_test import iperf3_speed_test


class BandwidthTest(Resource):
    """Resource for measuring bandwidth"""
    def get(self):
        return jsonify(
            download=RESULTS.get('download'),
            upload=RESULTS.get('upload'),
            download_data=RESULTS.get('download_data'),
            upload_data=RESULTS.get('upload_data'),
            speed_result_str=RESULTS.get('speed_result_str'),
            upload_mode=RESULTS.get('upload_mode'),
            download_mode=RESULTS.get('download_mode')
        )

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('server', type=str, help='Address of the testing server')
        parser.add_argument('duration', type=int, help='Duration of the iperf3 speed test in seconds')
        parser.add_argument('bandwidth', type=int, help='Duration of the iperf3 speed test in seconds')
        request_args = parser.parse_args()

        server = request_args.get('server')
        duration = request_args.get('duration')
        bandwidth = request_args.get('bandwidth')

        speed_results = iperf3_speed_test(
            server=server,
            bandwidth=str(bandwidth),
            duration=str(duration)
        )

        save_results(
            RESULTS,
            **speed_results,
        )

        return jsonify(
            upload=RESULTS['upload'],
            download=RESULTS['download'],
            upload_data=RESULTS['upload_data'],
            download_data=RESULTS['download_data'],
            speed_result_str=RESULTS['speed_result_str'],
            upload_mode=RESULTS['upload_mode'],
            download_mode=RESULTS['download_mode'],
            message='Test complete!'
        )
