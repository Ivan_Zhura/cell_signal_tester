from flask import jsonify
from flask_restful import Resource
from common.globals import RESULTS


class TestsData(Resource):
    """Resource for getting data for testing the quality of cellular communication"""
    def get(self):
        return jsonify(**RESULTS)
