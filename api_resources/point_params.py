"""
API for getting and setting name and coordinates of testing point
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from common.globals import RESULTS, save_results


class TestingPoint(Resource):
    """Resource for describing testing point parameters"""
    def get(self):
        point_name = RESULTS.get('name')
        try:
            point_lat = RESULTS['point_coords'][0]
            point_lon = RESULTS['point_coords'][1]
        except (KeyError, TypeError):
            point_lat, point_lon = None, None

        return jsonify(
            point_name=point_name,
            point_lat=point_lat,
            point_lon=point_lon
        )

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('point_name', type=str, help='Testing point name')
        parser.add_argument('point_lat', type=float, help='Testing point latitude')
        parser.add_argument('point_lon', type=float, help='Testing point longitude')
        request_args = parser.parse_args()

        point_name = request_args.get('point_name')
        point_lat = request_args.get('point_lat')
        point_lon = request_args.get('point_lon')
        point_coords = (point_lat, point_lon)

        save_results(
            RESULTS,
            name=point_name,
            point_coords=point_coords,
        )

        return jsonify(
            point_name=point_name,
            point_lat=point_lat,
            point_lon=point_lon,
            message='Point saved!'
        )

