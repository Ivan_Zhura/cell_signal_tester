"""
API for getting and setting testing server parameters
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from common.globals import RESULTS, save_results


class TestServer(Resource):
    """Resource for setting test server parameters"""
    def get(self):
        server_address = RESULTS.get('server')
        ping_test_packets_count = RESULTS.get('n_packets')
        packet_size = RESULTS.get('packet_size')
        duration_of_speed_test = RESULTS.get('duration')
        try:
            test_bandwidth = int(RESULTS.get('bandwidth') / 1000)
        except TypeError:
            test_bandwidth = None
        return jsonify(
            server=server_address,
            n_packets=ping_test_packets_count,
            packet_size=packet_size,
            duration=duration_of_speed_test,
            bandwidth=test_bandwidth
        )

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('server', type=str, help='Address of the testing server')
        parser.add_argument('n_packets', type=int, help='Number of ping-test packets')
        parser.add_argument('packet_size', type=int, help='Number of data bytes to be sent')
        parser.add_argument('duration', type=int, help='Duration of the iperf3 speed test in seconds')
        parser.add_argument('bandwidth', type=int, help='Duration of the iperf3 speed test in seconds')
        request_args = parser.parse_args()

        server = request_args.get('server')
        n_packets = request_args.get('n_packets')
        packet_size = request_args.get('packet_size')
        duration = request_args.get('duration')
        try:
            bandwidth = request_args.get('bandwidth') * 1000
        except TypeError:
            bandwidth = None

        save_results(
            RESULTS,
            server=server,
            n_packets=n_packets,
            packet_size=packet_size,
            duration=duration,
            bandwidth=bandwidth
        )

        return jsonify(
            server=server,
            n_packets=n_packets,
            packet_size=packet_size,
            duration=duration,
            bandwidth=bandwidth,
            message='Server saved!'
        )
