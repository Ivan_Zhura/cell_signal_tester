"""
API for analysis of the first Fresnel zone
"""
from flask_restful import Resource
from flask_restful import reqparse
from flask import jsonify
from common.fresnel_backend import plot_fresnel_zone
from common.globals import (
    save_results,
    RESULTS
)

# TODO: сделать возвращение словаря результатов построения аналогично ping и speed

INIT_OBJECT_COORDS = (None, None)
INIT_BS_COORDS = (None, None)
INIT_FREQUENCY = None
INIT_BS_HEIGHT = None
INIT_ANT_HEIGHT = None
MONITORING_PROCESS = None


class FresnelZone(Resource):
    """Resource for plotting Fresnel zone"""

    def get(self):
        return jsonify(
            fresnel_image=RESULTS.get('fresnel_image'),
            bs_height=RESULTS['bs_height'],
            ant_height=RESULTS['ant_height'],
            freq=RESULTS.get('freq'),
            dist=RESULTS.get('dist'),
            fresnel_60_top_overlap=RESULTS.get('fresnel_60_top_overlap'),
            fresnel_60_bottom_overlap=RESULTS.get('fresnel_60_bottom_overlap'),
            direct_line_overlap=RESULTS.get('direct_line_overlap')
        )

    def post(self):
        global INIT_OBJECT_COORDS, INIT_BS_COORDS, INIT_FREQUENCY, INIT_ANT_HEIGHT, INIT_BS_HEIGHT
        # request's args for parsing:
        parser = reqparse.RequestParser()
        parser.add_argument('point_lat', type=float, help='Testing point latitude')
        parser.add_argument('point_lon', type=float, help='Testing point longitude')
        parser.add_argument('bs_lat', type=float, help='Base station latitude')
        parser.add_argument('bs_lon', type=float, help='Base station longitude')
        parser.add_argument('freq', type=float, help='Signal frequency')
        parser.add_argument('bs_height', type=float, help='Base station height')
        parser.add_argument('ant_height', type=float, help='Antenna mounting height')
        request_args = parser.parse_args()
        # parsing request's args:
        point_coords = (request_args.get('point_lat'), request_args.get('point_lon'))
        bs_coords = (request_args.get('bs_lat'), request_args.get('bs_lon'))
        freq = request_args.get('freq')
        bs_height = request_args.get('bs_height')
        point_ant_height = request_args.get('ant_height')
        # caching conditions:
        input_data_was_not_updated = point_coords == INIT_OBJECT_COORDS and \
                                     bs_coords == INIT_BS_COORDS and \
                                     freq == INIT_FREQUENCY and \
                                     bs_height == INIT_BS_HEIGHT and \
                                     point_ant_height == INIT_ANT_HEIGHT and \
                                     'fresnel_image' in RESULTS
        # return cached data:
        if input_data_was_not_updated:
            return jsonify(
                fresnel_image=RESULTS['fresnel_image'],
                bs_height=RESULTS['bs_height'],
                point_ant_height=RESULTS['ant_height'],
                freq=RESULTS['freq'],
                dist=RESULTS['dist'],
                fresnel_60_top_overlap=RESULTS['fresnel_60_top_overlap'],
                fresnel_60_bottom_overlap=RESULTS['fresnel_60_bottom_overlap'],
                direct_line_overlap=RESULTS['direct_line_overlap']
            )
        # plotting Fresnel zone:
        fresnel_plot_data = plot_fresnel_zone(
            point_coords,
            bs_coords,
            freq,
            bs_height,
            point_ant_height,
            RESULTS
        )
        save_results(RESULTS, fresnel_image=fresnel_plot_data)
        # update data for caching:
        INIT_BS_COORDS = bs_coords
        INIT_OBJECT_COORDS = point_coords
        INIT_FREQUENCY = freq
        INIT_ANT_HEIGHT = point_ant_height
        INIT_BS_HEIGHT = bs_height
        return jsonify(
            fresnel_image=fresnel_plot_data,
            bs_height=RESULTS['bs_height'],
            point_ant_height=RESULTS['ant_height'],
            freq=RESULTS['freq'],
            dist=RESULTS['dist'],
            fresnel_60_top_overlap=RESULTS['fresnel_60_top_overlap'],
            fresnel_60_bottom_overlap=RESULTS['fresnel_60_bottom_overlap'],
            direct_line_overlap=RESULTS['direct_line_overlap'],
            message='Fresnel zone drawn!'
        )
