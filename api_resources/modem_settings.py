"""
API resource for getting and setting modem's net type
"""
from flask import jsonify
from flask_restful import Resource
from flask_restful import reqparse
from requests.exceptions import ConnectionError
from common.globals import RESULTS, save_results
from common.modems.modem_controller import modem


class ModemSettings(Resource):
    """Resource for setting the network type and frequency on the Huawei E3372h modem"""

    def get(self):
        net_mode = None
        error = None
        try:
            net_mode = modem.get_network_mode()
        except (ConnectionError, AttributeError):
            error = 'No connection to modem! Please check device performance.'

        save_results(
            RESULTS,
            net_mode=net_mode,
            modem_type=str(modem),
            error=error
        )

        return jsonify(
            net_mode=net_mode,
            modem_type=str(modem),
            error=error
        )

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('net_type', type=str, help='Type of network under test')
        request_args = parser.parse_args()
        net_type = request_args.get('net_type')
        modem.set_network_type(net_type)
        freq = modem.get_current_frequency()
        return jsonify(
            net_mode=net_type,
            freq=freq,
            message='Best band found!'
        )
