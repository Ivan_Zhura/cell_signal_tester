"""
Python script for downloading SRTM3 data
"""
import os
import io
import zipfile
import requests
from bs4 import (
    BeautifulSoup,
    SoupStrainer
)


BASE_URL = 'https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/'
# comment on continents you don't need (use # for comment):
CONTINENTS = [
    'Africa/',
    'Australia/'
    'Eurasia/',
    'Islands/',
    'North_America/',
    'South_America/'
]

os.chdir(os.path.join('..', 'common', 'data', 'srtm_data'))

for continent in CONTINENTS:
    os.mkdir(continent)
    print(f'Downloading data for the continent: {continent.rstrip("/")}')
    print('_______________________________________')
    r = requests.get(BASE_URL + continent)
    soup = BeautifulSoup(r.content, 'html.parser', parse_only=SoupStrainer('a'))
    urls = [link['href'] for link in soup if link.get('href')]
    for index, url in enumerate(urls[1:]):
        r = requests.get(BASE_URL + continent + url)
        with r, zipfile.ZipFile(io.BytesIO(r.content)) as archive:
            archive.extractall(os.path.join(os.getcwd(), continent))
            print(index + 1, 'file downloaded:', url.rstrip('.zip'), sep=' ')
    print(f'Loading data for the continent {continent[:-1]} complete!')
    print()
    print('_______________________________________')

print('Downloading SRTM3 data complete!')
