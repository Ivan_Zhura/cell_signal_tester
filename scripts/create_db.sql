CREATE TABLE objects
(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(24) NOT NULL,
    point_lat NUMERIC (7, 5),
    point_lon NUMERIC (7, 5),
    UNIQUE (name, point_lat, point_lon)
);


CREATE TABLE tests
(
    id SERIAL PRIMARY KEY,
    object_id INTEGER,
    server CHARACTER VARYING(24),
    error CHARACTER VARYING(128),
    modem_type CHARACTER VARYING(32),
    datetime TIMESTAMP,

    fresnel_image JSON,
    bs_coords JSON,
    dist NUMERIC,
    bs_height NUMERIC (7, 3),
    ant_height NUMERIC (5, 3),
    fresnel_60_top_overlap BOOLEAN,
    fresnel_60_bottom_overlap BOOLEAN,
    direct_line_overlap BOOLEAN,
    delta_point NUMERIC,
    delta_bs NUMERIC,
    point_bs_h NUMERIC,

    n_packets SMALLINT,
    packet_size SMALLINT,
    min_ping NUMERIC (9, 3),
    max_ping NUMERIC (9, 3),
    mean_ping NUMERIC (9, 3),
    p_loss NUMERIC (6, 3),
    count_max SMALLINT,
    count_max_percent NUMERIC (6, 3),
    ping_list JSON,
    ping_raw_results TEXT,

    bandwidth INTEGER,
    duration SMALLINT,
    download_data JSON,
    upload_data JSON,
    download NUMERIC (9, 3),
    upload NUMERIC (9, 3),
    speed_result_str TEXT,
    upload_mode CHARACTER VARYING(10),
    download_mode CHARACTER VARYING(10),

    rssi SMALLINT,
    asu SMALLINT,
    cellid INTEGER,
    lac INTEGER,
    freq SMALLINT,
    signal_test BOOLEAN,
    operator_code CHARACTER VARYING(12),
    operator_name CHARACTER VARYING(24),
    net_type CHARACTER VARYING(8),
    net_mode CHARACTER VARYING(8),

    FOREIGN KEY (object_id) REFERENCES objects (id)
);

GRANT ALL PRIVILEGES ON DATABASE "cell_signal_tester" to postgres;
