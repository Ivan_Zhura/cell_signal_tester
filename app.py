"""WSGI application module"""
from flask import (
    Flask,
    request,
    redirect,
    session,
    render_template
)
from flask_restful import Api
from common.globals import RESULTS
from api_resources.point_params import TestingPoint
from api_resources.testing_server_params import TestServer
from api_resources.fresnel_zone import FresnelZone
from api_resources.speed import BandwidthTest
from api_resources.ping import PingTest
from api_resources.get_full_data import TestsData
from api_resources.modem_settings import ModemSettings
from api_resources.save_tests_data import Storage
from api_resources.signal import (
    Signal,
    SignalDetailed
)
from api_resources.bs_list import (
    BaseStationList,
    BaseStationCoordinates
)
from common.user_checker import check_logged_in
from common.modems.modem_controller import modem
from common.config import (
    USERNAME,
    PASSWORD
)
from common.forms import (
    LoginForm,
    PointForm,
    ModemSettingsForm,
    TestServerForm,
    PingTestForm,
    SpeedTestForm,
    SignalParamsForm,
    FresnelForm
)

# configure WSGI-APP
app = Flask(__name__)
app.config.from_pyfile('config.py')

# configure REST API
api = Api(app)
api.add_resource(TestingPoint, '/api/v1.0/point')
api.add_resource(TestServer, '/api/v1.0/server')
api.add_resource(SignalDetailed, '/api/v1.0/signal')
api.add_resource(Signal, '/api/v1.0/simple_signal')
api.add_resource(BaseStationList, '/api/v1.0/bs_list')
api.add_resource(BaseStationCoordinates, '/api/v1.0/bs_coords')
api.add_resource(FresnelZone, '/api/v1.0/fresnel_zone')
api.add_resource(BandwidthTest, '/api/v1.0/speed')
api.add_resource(PingTest, '/api/v1.0/ping')
api.add_resource(ModemSettings, '/api/v1.0/modem_settings')
api.add_resource(TestsData, '/api/v1.0/tests_data')
api.add_resource(Storage, '/api/v1.0/save_data')


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            username = form.username.data
            password = form.password.data
            if password == PASSWORD and username == USERNAME:
                session['logged_in'] = True
                return redirect('/point')
            session['logged_in'] = False
        return render_template('login.html', title='Авторизация', form=form)
    else:
        if 'logged_in' in session:
            return redirect('/point')
        return render_template(
            'login.html',
            title='Авторизация',
            form=form
        )


@app.route('/point', methods=['GET', 'POST'])
@check_logged_in
def main_params():
    if request.method == 'GET':
        point_form = PointForm()
        return render_template(
            'main_params.html',
            title='Обследуемый объект',
            point_form=point_form,
        )
    return redirect('/server')


@app.route('/server', methods=['GET', 'POST'])
@check_logged_in
def server_params():
    if request.method == 'GET':
        server_form = TestServerForm()
        ping_params_form = PingTestForm()
        speed_params_form = SpeedTestForm()
        return render_template(
            'server_params.html',
            title='Тестовый сервер',
            server_form=server_form,
            ping_params_form=ping_params_form,
            speed_params_form=speed_params_form
        )
    else:
        if modem is not None:
            return redirect('/modem_config')
        return redirect('/tests')


@app.route('/modem_config')
@check_logged_in
def modem_config():
    modem_form = ModemSettingsForm()
    return render_template(
        'modem_settings.html',
        title='Настройки модема',
        modem_form=modem_form,
        modem=str(modem)
    )


@app.route('/signal', methods=['GET', 'POST'])
@check_logged_in
def bs_params():
    if request.method == 'GET':
        signal_form = SignalParamsForm()
        return render_template(
            'bs_params.html',
            title='Базовая станция',
            signal_form=signal_form,
            modem=str(modem)
        )
    return redirect('/tests')


@app.route('/tests', methods=['GET', 'POST'])
@check_logged_in
def tests():
    if not RESULTS['point_coords'][0]:
        return redirect('/point')
    # if not RESULTS['bs_coords'][0]:
    #     return redirect('/signal')
    point_form = PointForm()
    server_form = TestServerForm()
    ping_test_form = PingTestForm()
    speed_test_form = SpeedTestForm()
    signal_form = SignalParamsForm()
    fresnel_form = FresnelForm()
    return render_template(
        'autotest.html',
        title='Тестирование качества связи',
        point_form=point_form,
        server_form=server_form,
        ping_test_form=ping_test_form,
        speed_test_form=speed_test_form,
        signal_form=signal_form,
        fresnel_form=fresnel_form,
        modem=str(modem),
        **RESULTS
    )


@app.route('/monitoring', methods=['GET', 'POST'])
@check_logged_in
def monitoring():
    return render_template('monitoring.html')


@app.route('/history')
@check_logged_in
def history():
    return render_template('manual.html')


@app.route('/manual')
@check_logged_in
def manual():
    return render_template('manual.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
