function getServerParams() {
    var request = new XMLHttpRequest();
    document.getElementById('ip_address').focus()
    request.open('GET', 'api/v1.0/server', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('ip_address').value = data.server;
            document.getElementById('speed').value = data.bandwidth;
            document.getElementById('duration').value = data.duration;
            document.getElementById('num_packets').value = data.n_packets;
            document.getElementById('packet_size').value = data.packet_size;
        } else {
            alert('Сервер вернул ошибку!')
        }
    };
    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function postServerParams() {
    var request = new XMLHttpRequest();
    request.open('POST', 'api/v1.0/server', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    let body = "server=" + document.getElementById('ip_address').value +
        "&bandwidth=" + document.getElementById('speed').value +
        "&duration=" + document.getElementById('duration').value +
        "&n_packets=" + document.getElementById('num_packets').value +
        "&packet_size=" + document.getElementById('packet_size').value
    request.send(body);
    alert('Параметры успешно сохранены!')
}