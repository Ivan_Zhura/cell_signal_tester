"use strict";

function speedTest() {
    var request = new XMLHttpRequest();
    request.open('POST', '/api/v1.0/speed', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    restoreDefaultSpeedContainerStyles()
    document.getElementById('speed_test').hidden = false;
    document.getElementById('speed_metrics_container').hidden = true;
    document.getElementById('speed_loading').hidden = false;
    document.getElementById('speedChart').style.display = 'none';
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            let data = JSON.parse(this.response)
            showSpeedTestResults(data)
            verifySpeedTestResults(data)
            saveResults()
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };

    let body = "server=" + document.getElementById('ip_address').value +
        "&duration=" + document.getElementById('duration').value +
        "&bandwidth=" + document.getElementById('speed').value
    request.send(body);
}

function showSpeedTestResults(data) {
    document.getElementById('speed_test').hidden = false;
    document.getElementById('speed_loading').hidden = true;
    document.getElementById('speed_results').value = data.speed_result_str;
    document.getElementById('speed_metrics_container').hidden = false;
    document.getElementById('speed_metrics').innerHTML =
        `Средняя скорость (на сервер): ${data.upload} КБит/c<br>
Средняя скорость (с сервера): ${data.download} КБит/c<br>
Мода (на сервер): ${data.upload_mode} КБит/c<br>
Мода (с сервера): ${data.download_mode} КБит/c`
    deleteOldSpeedChart()
    plotSpeed(data)
}

function verifySpeedTestResults(data) {
    if (data.upload_mode === 0 || data.download_mode === 0) {
        speedTestFailed()
    } else {
        speedTestPassed()
    }
}

function speedTestFailed() {
    document.getElementById('speed_color').style.backgroundColor = 'rgba(255,0,0,0.3)';
    document.getElementById('speed_result').innerText = 'Тест не пройден!'
    document.getElementById('speed_result').style.color = 'red'
}

function speedTestPassed() {
    document.getElementById('speed_color').style.backgroundColor = 'rgba(102,255,204,1)';
    document.getElementById('speed_result').innerText = 'Тест пройден!'
    document.getElementById('speed_result').style.color = 'black'
}

function restoreDefaultSpeedContainerStyles () {
    document.getElementById('speed_color').style.backgroundColor = null;
    document.getElementById('speed_result').innerText = null;
    document.getElementById('speed_result').style.color = null;

}

function deleteOldSpeedChart() {
    document.getElementById('speed_line_chart').remove()
    let newchart = document.createElement('div')
    newchart.id = 'speed_line_chart'
    document.getElementById('speedChart').appendChild(newchart)
    document.getElementById('speedChart').style.display = 'block';
}

function plotSpeed(data) {
    let speedChart = document.getElementById('speed_line_chart');
    let duration = parseInt(document.getElementById('duration').value)
    let upload = {
        x: Array.from(Array(duration).keys()),
        y: data.upload_data,
        type: 'scatter',
        color: 'green',
        name: 'На сервер, КБит/с',
        fill: 'tozeroy'
    };

    let download= {
        x: Array.from(Array(duration).keys()),
        y: data.download_data,
        type: 'scatter',
        color: 'green',
        name: 'С сервера, КБит/с',
        fill: 'tozeroy'
    };
    let plot_data = [upload, download];
    let layout = {
        colorway : ['green', 'red'],
        font: {
            size: 16,
        },
        plot_bgcolor: 'rgba(0,0,0,0)',
        paper_bgcolor: 'rgba(0,0,0,0)',
        margin: {
            pad: 10
        },
        xaxis: {
            title: 'Продолжительность теста, с',
            titlefont: {
                size: 12
            },
        },
        yaxis: {
            title: 'Пропускная способность, КБит/с',
            titlefont: {
                size: 12
            },
            rangemode: 'tozero'
        },
        title: false,
        legend: {
            orientation: 'h',
            x: 0,
            y: -0.2,
            font: {
                size: 12,
            }
        }
    };
    let config = {
        responsive: true,
        displayModeBar: false
    }
    Plotly.plot(speedChart, plot_data, layout, config);
}