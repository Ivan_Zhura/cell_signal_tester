"use strict";

function startFresnelTest() {
    changeRunButtonText()
    var request = new XMLHttpRequest();
    request.open('POST', 'api/v1.0/fresnel_zone', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    restoreDefaultFresnelContainerStyles()
    document.getElementById('fresnel_test').hidden = false;
    document.getElementById('fresnel_metrics_container').hidden = true;
    document.getElementById('fresnel_loading').hidden = false;
    document.getElementById('fresnel_zone').style.display = 'none';
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            let data = JSON.parse(this.response)
            showFresnelTestResults(data)
            plotFresnelZone(data.fresnel_image)
            deleteOldFresnelChart()
            verifyFresnelZoneResults(data)
            pingTest()
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };

    let body = "point_lat=" + document.getElementById('point_lat').value +
        "&point_lon=" + document.getElementById('point_lon').value +
        "&bs_lat=" + document.getElementById('bs_lat').value +
        "&bs_lon=" + document.getElementById('bs_lon').value +
        "&freq=" + document.getElementById('freq').value +
        "&bs_height=" + document.getElementById('bs_height').value +
        "&ant_height=" + document.getElementById('ant_height').value

    request.send(body);
}

function showFresnelTestResults(data) {
    document.getElementById('fresnel_test').hidden = false;
    document.getElementById('fresnel_loading').hidden = true;
    document.getElementById('fresnel_zone').style.display = 'flex';
    document.getElementById('fresnel_metrics_container').hidden = false;

    let fresnel_60_top_overlap
    let fresnel_60_bottom_overlap
    let direct_line_overlap
    if (data.fresnel_60_top_overlap) {
        fresnel_60_top_overlap = 'Есть'
    } else {
        fresnel_60_top_overlap = 'Отсутствует'
    }
    if (data.fresnel_60_bottom_overlap) {
        fresnel_60_bottom_overlap = 'Есть'
    } else {
        fresnel_60_bottom_overlap = 'Отсутствует'
    }
    if (data.direct_line_overlap) {
        direct_line_overlap = 'Есть'
    } else {
        direct_line_overlap = 'Отсутствует'
    }
    document.getElementById('fresnel_metrics').innerHTML =
        `Дистанция до базовой станции: ${data.dist} км.<br>
Пересечение верхней границы: ${fresnel_60_top_overlap}<br>
Пересечение линии прямой видимости: ${fresnel_60_bottom_overlap}<br>
Пересечение нижней границы: ${direct_line_overlap}`
}

function verifyFresnelZoneResults(data) {
    if (data.fresnel_60_top_overlap) {
        fresnelTestFailed()
    }
    if (data.freq < 1000) {
        if (data.direct_line_overlap) {
            if (data.dist <= 6) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        } else if (data.fresnel_60_bottom_overlap) {
            if (data.dist <= 10) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        } else {
            if (data.dist <= 12) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        }
    } else if (data.freq > 1000) {
        if (data.direct_line_overlap) {
            if (data.dist <= 2) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        } else if (data.fresnel_60_bottom_overlap) {
            if (data.dist <= 6) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        } else {
            if (data.dist <= 8) {
                fresnelTestPassed()
            } else {
                fresnelTestFailed()
            }
        }
    }
    document.getElementById('fresnel_result').hidden = false;
}

function fresnelTestFailed() {
    document.getElementById('fresnel_color').style.backgroundColor = 'rgba(255,0,0,0.3)';
    document.getElementById('fresnel_result').innerText = 'Тест не пройден!'
    document.getElementById('fresnel_result').style.color = 'red'
}

function fresnelTestPassed() {
    document.getElementById('fresnel_color').style.backgroundColor = 'rgba(102,255,204,1)';
    document.getElementById('fresnel_result').innerText = 'Тест пройден!'
    document.getElementById('fresnel_result').style.color = 'black'
}

function restoreDefaultFresnelContainerStyles () {
    document.getElementById('fresnel_color').style.backgroundColor = null;
    document.getElementById('fresnel_result').innerText = null;
    document.getElementById('fresnel_result').style.color = null;

}

function deleteOldFresnelChart() {
    document.getElementById('fresnel_chart').remove()
    let newchart = document.createElement('div')
    newchart.id = 'fresnel_chart'
    document.getElementById('fresnel_zone').appendChild(newchart)
    document.getElementById('fresnel_zone').style.display = 'block';
}

function plotFresnelZone(fresnelData) {
    let fresnelChart = document.getElementById('fresnel_zone');
    let heights = {
        x: fresnelData.x_earth,
        y: fresnelData.heights,
        type: 'scatter',
        color: 'green',
        name: 'Рельеф',
        fill: 'tozeroy'
    };

    let fresnel = {
        x: fresnelData.x,
        y: fresnelData.y,
        type: 'scatter',
        name: 'I зона Френеля',
        fill: 'tozeroy'
    }

    let fresnel_60 = {
        x: fresnelData.x60,
        y: fresnelData.y60,
        type: 'scatter',
        name: '60% I зоны Френеля',
        fill: 'tozeroy'
    }

    let point_bs = {
        type: 'scatter',
        mode: 'lines+markers',
        x: [fresnelData.point[0], fresnelData.bs[0]],
        y: [fresnelData.point[1], fresnelData.bs[1]],
        name: 'Линия прямой видимости',
        marker: {
            size: [30, 30],
            sizemode: 'area'
        }
    };

    let bad_markers = {
        type: 'scatter',
        mode: 'markers',
        x: fresnelData.bad_markers_x,
        y: fresnelData.bad_markers_y,
        name: 'Точки перекрытия',
        marker: {
            size: 10,
            symbol: 'x'
        }
    };


    let data = [heights, fresnel, fresnel_60, point_bs, bad_markers];

    let layout = {
        colorway : ['green', 'blue', 'yellow', 'red', 'brown'],
        font: {
            size: 16,
        },
        plot_bgcolor: 'rgba(0,0,0,0)',
        paper_bgcolor: 'rgba(0,0,0,0)',
        margin: {
            pad: 10
        },
        xaxis: {
            title: 'Удаленность от места установки, м',
            titlefont: {
                size: 12
            },
        },
        yaxis: {
            title: 'Высота над уровнем моря, м',
            titlefont: {
                size: 12
            },
            rangemode: 'tozero'
        },
        title: false,
        legend: {
            orientation: 'h',
            x: 0,
            y: -0.2,
            font: {
                size: 12,
            }
        }
    };
    let config = {
        responsive: true,
        displayModeBar: false
    }
    Plotly.plot(fresnelChart, data, layout, config);
}
