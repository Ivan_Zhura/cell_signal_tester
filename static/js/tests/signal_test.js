"use strict";

function signalTest() {
    var request = new XMLHttpRequest();
    request.open('GET', 'api/v1.0/simple_signal', true);
    changeRunButtonText()
    restoreDefaultSignalContainerStyles()
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            showSignalTestResults(data);
            plotSignalStrenth(data.rssi);
            verifySignalTestResults(data);
            startFresnelTest();
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function plotSignalStrenth(signal) {
    let chartDiv = document.getElementById('signal_picture');
    let bar_color;

    if (signal >= -75) {
        bar_color = 'green'
    } else if (signal < -75 && signal >= -85) {
        bar_color = 'yellow'
    } else if (signal < -85 && signal >= -95) {
        bar_color = 'orange'
    } else if (signal < -95 ){
        bar_color = 'red'
    }

    let data = [
        {
            value: signal,
            title: {text: "Уровень сигнала" },
            type: "indicator",
            mode: "gauge+number",
            gauge: {
                axis: {range: [-113, -60]},
                bar: {
                    color: bar_color
                }
            },
            number: {suffix: ' dBm'},
            delta: {reference: -85},
        }
    ];
    var layout = {
        plot_bgcolor: 'rgba(0,0,0,0)',
        paper_bgcolor: 'rgba(0,0,0,0)',
    }
    let config = {
        responsive: true,
        displayModeBar: false
    }
    Plotly.newPlot(chartDiv, data, layout, config);
}

function showSignalTestResults(data) {
    document.getElementById('signal_test').hidden = false;
    let signal_info = document.getElementById('signal_info')
    signal_info.innerHTML = `Оператор: <b>${data.operator_name}</b><br>Тип сети: <b>${data.net_type}</b>`
}

function verifySignalTestResults(test_rssi) {
    if (test_rssi < -85) {
        signalTestFailed()
    } else {
        signalTestPassed()
    }
}

function signalTestFailed() {
    document.getElementById('signal_color').style.backgroundColor = 'rgba(255,0,0,0.3)';
    document.getElementById('signal_result').innerText = 'Тест не пройден!'
    document.getElementById('signal_result').style.color = 'red'
}

function signalTestPassed() {
    document.getElementById('signal_color').style.backgroundColor = 'rgba(102,255,204,1)';
    document.getElementById('signal_result').innerText = 'Тест пройден!'
    document.getElementById('signal_result').style.color = 'black'
}

function restoreDefaultSignalContainerStyles () {
    document.getElementById('signal_color').style.backgroundColor = null;
    document.getElementById('signal_result').innerText = null;
    document.getElementById('signal_result').style.color = null;

}