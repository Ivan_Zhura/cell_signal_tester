"use strict";

function pingTest() {
    var request = new XMLHttpRequest();
    request.open('POST', 'api/v1.0/ping', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    restoreDefaultPingContainerStyles()
    document.getElementById('ping_test').hidden = false;
    document.getElementById('ping_metrics_container').hidden = true;
    document.getElementById('ping_loading').hidden = false;
    document.getElementById('pingChart').style.display = 'none';
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            let data = JSON.parse(this.response)
            showPingTestResults(data)
            verifyPingTestResults(data)
            speedTest()
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };

    let body = "server=" + document.getElementById('ip_address').value +
        "&n_packets=" + document.getElementById('num_packets').value +
        "&packet_size=" + document.getElementById('packet_size').value
    request.send(body);
}

function showPingTestResults(data) {
    document.getElementById('ping_test').hidden = false;
    document.getElementById('ping_loading').hidden = true;
    document.getElementById('ping_results').value = data.ping_raw_results;
    document.getElementById('ping_metrics_container').hidden = false;
    document.getElementById('ping_metrics').innerHTML =
        `Максимальный пинг: ${data.max_ping} мс<br>
Средний пинг: ${data.mean_ping} мс<br>
Минимальный пинг: ${data.min_ping} мс<br>
Потеря пакетов: ${data.p_loss} %<br>
Пакеты с большим значением пинга: ${data.count_max_percent} %<br>`
    deleteOldPingChart()
    plotPing(data)
}

function verifyPingTestResults(data) {
    if (data.max_ping <= 3700 && data.p_loss < 1 && data.count_max_percent <= 19) {
        pingTestPassed()
    } else {
        pingTestFailed()
    }
}

function pingTestFailed() {
    document.getElementById('ping_color').style.backgroundColor = 'rgba(255,0,0,0.3)';
    document.getElementById('ping_result').innerText = 'Тест не пройден!'
    document.getElementById('ping_result').style.color = 'red'
}

function pingTestPassed() {
    document.getElementById('ping_color').style.backgroundColor = 'rgba(102,255,204,1)';
    document.getElementById('ping_result').innerText = 'Тест пройден!'
    document.getElementById('ping_result').style.color = 'black'
}

function restoreDefaultPingContainerStyles() {
    document.getElementById('ping_color').style.backgroundColor = null;
    document.getElementById('ping_result').innerText = null;
    document.getElementById('ping_result').style.color = null;

}

function deleteOldPingChart() {
    document.getElementById('ping_line_chart').remove()
    let newchart = document.createElement('div')
    newchart.id = 'ping_line_chart'
    document.getElementById('pingChart').appendChild(newchart)
    document.getElementById('pingChart').style.display = 'block';
}

function plotPing(data) {
    let pingChart = document.getElementById('ping_line_chart');
    let num_packets = parseInt(document.getElementById('num_packets').value)
    let ping = {
        x: Array.from(Array(num_packets).keys()),
        y: data.ping_list,
        type: 'scatter',
        color: 'green',
        name: 'Пинг, мс',
        fill: 'tozeroy'
    };
    let mean_ping = {
        x: [0, num_packets - 1],
        y: [data.mean_ping, data.mean_ping],
        type: 'scatter',
        mode: 'lines',
        color: 'red',
        name: 'Средний пинг, мс',
    }
    let plot_data = [ping, mean_ping];
    let layout = {
        colorway : ['green', 'red'],
        font: {
            size: 16,
        },
        plot_bgcolor: 'rgba(0,0,0,0)',
        paper_bgcolor: 'rgba(0,0,0,0)',
        margin: {
            pad: 10
        },
        xaxis: {
            title: 'Пакет №',
            titlefont: {
                size: 12
            },
        },
        yaxis: {
            title: 'Пинг, мс',
            titlefont: {
                size: 12
            },
            rangemode: 'tozero'
        },
        title: false,
        legend: {
            orientation: 'h',
            x: 0,
            y: -0.2,
            font: {
                size: 12,
            }
        }
    };
    let config = {
        responsive: true,
        displayModeBar: false
    }
    Plotly.plot(pingChart, plot_data, layout, config);
}