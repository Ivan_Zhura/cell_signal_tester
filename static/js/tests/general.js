"use strict";

function hideTestsDivs() {
    document.getElementById('signal_test').hidden = true;
    document.getElementById('fresnel_test').hidden = true;
    document.getElementById('ping_test').hidden = true;
    document.getElementById('speed_test').hidden = true;
}

function prepareTestsPage() {
    hideTestsDivs()
    document.getElementById('run_tests_button').innerText = 'Начать тестирование'

    var request = new XMLHttpRequest();
    request.open('GET', 'api/v1.0/tests_data', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            if (data.signal_test){
                showSignalTestResults(data)
                plotSignalStrenth(data.rssi)
                verifySignalTestResults(data.rssi)
            }
            if (data.fresnel_image) {
                showFresnelTestResults(data)
                deleteOldFresnelChart()
                plotFresnelZone(data.fresnel_image)
                verifyFresnelZoneResults(data)
            }
            if (data.ping_raw_results) {
                showPingTestResults(data)
                deleteOldPingChart()
                plotPing(data)
                verifyPingTestResults(data)
            }
            if (data.speed_result_str) {
                showSpeedTestResults(data)
                deleteOldSpeedChart()
                plotSpeed(data)
                verifySpeedTestResults(data)
            }
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с сервером!')
    };
    request.send();
}

function changeRunButtonText() {
    document.getElementById('run_tests_button').innerText = 'Повторить тесты'
}

function getBaseStationCoords() {
    var request = new XMLHttpRequest();
    request.open('GET', 'api/v1.0/bs_coords', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('bs_lat').value = data.bs_coords[0];
            document.getElementById('bs_lon').value = data.bs_coords[1];

        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function getFresnelParams() {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1.0/fresnel_zone', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('ant_height').value = data.ant_height;
            document.getElementById('bs_height').value = data.bs_height;
            if (data.freq) {
                document.getElementById('freq').value = data.freq;
            }
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}