function createCheckBox(name, value, id, parent) {
    var container = document.createElement('div');
    container.classList.add('form-check', 'my-2', 'col-md-12')
    container.style.paddingLeft = '0';

    var checkbox = document.createElement('input');
    checkbox.type = "radio";
    checkbox.name = name;
    checkbox.value = value;
    checkbox.id = id;
    checkbox.onclick = function () {
        let bs_coords = this.value.split(',')
        document.getElementById('bs_lat').value = bs_coords[0];
        document.getElementById('bs_lon').value = bs_coords[1];
    }
    var label = document.createElement('label')
    label.htmlFor = id;
    label.appendChild(document.createTextNode(value[0] + '; ' + value[1]));
    label.classList.add('mx-2')

    container.appendChild(checkbox);
    container.appendChild(label);
    parent.appendChild(container);
}

function getBaseStationList() {
    var request = new XMLHttpRequest();

    request.open('GET', 'api/v1.0/bs_list', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('loading_bs_list').hidden = false;
            if (data.bs_list.length > 0) {
                document.getElementById('bs_list_title').style.display = 'block';
                let bs_list_container = document.getElementById('bs_list')
                for (let i = 0; i < data.bs_list.length; i++) {
                    createCheckBox(`bs_coords`, data.bs_list[i], `bs-${i}`, bs_list_container);
                };
                if (data.bs_list.length === 1) {
                    document.getElementById('bs_lat').value = data.bs_lat;
                    document.getElementById('bs_lon').value = data.bs_lon;
                }
            } else {
                document.getElementById('bs_list_title').style.display = 'none';
                document.getElementById('bs_lat').value = 0;
                document.getElementById('bs_lon').value = 0;
                document.getElementById('bs_list_warning').style.display = 'block'
            }
            document.getElementById('loading_bs_list').hidden = true;
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function postBaseStationCoords() {
    var request = new XMLHttpRequest();
    request.open('POST', 'api/v1.0/bs_list', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    let body = "bs_lat=" + document.getElementById('bs_lat').value +
        "&bs_lon=" + document.getElementById('bs_lon').value
    request.send(body);
    alert('Координаты БС сохранены!')
}