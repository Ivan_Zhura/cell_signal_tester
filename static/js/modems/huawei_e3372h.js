const net_types = {
    'auto': 'net_type-0',
    '2g': 'net_type-1',
    '3g': 'net_type-2',
    'lte': 'net_type-3'
};

function getModemSettings() {
    var request = new XMLHttpRequest();
    let loading_gif = document.getElementById('loading_modem')

    request.open('GET', 'api/v1.0/modem_settings', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            loading_gif.hidden = true;
            document.getElementById('form_body').style.display = 'block';

            if (data.error) {
                document.getElementById('modem_error').style.display = 'block'
            }
            else  {
                document.getElementById(net_types[data.net_mode]).checked = true;
            }

        } else {
            loading_gif.hidden = true;
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function setModemSettings() {
    var request = new XMLHttpRequest();
    let loading_gif = document.getElementById('loading_freq');
    let selectedNetType = document.querySelector('input[name="net_type"]:checked').getAttribute('value');
    loading_gif.hidden = false;
    request.open('PUT', 'api/v1.0/modem_settings?net_type=' + selectedNetType, true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById(net_types[data.net_mode]).checked = true;
            // loading_gif.hidden = true;
            if (data.freq) {
                alert(`Наилучшая частота для выбранного типа сети составляет ${data.freq} МГц`)
            }
            window.location.assign('/signal');
        } else {
            alert('Модем не опознан!')
            // loading_gif.hidden = true;
            window.location.assign('/tests');
        }
    };
    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

getModemSettings();
