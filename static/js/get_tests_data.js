function getTestsData() {
    document.getElementById('fresnel_test').hidden = true;
    document.getElementById('ping_test').hidden = true;
    document.getElementById('speed_test').hidden = true;

    var request = new XMLHttpRequest();
    request.open('GET', 'api/v1.0/tests_data', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('point_name').value = data.name;
            document.getElementById('point_lat').value = data.point_coords[0];
            document.getElementById('point_lon').value = data.point_coords[1];
            document.getElementById('bs_lat').value = data.bs_coords[0];
            document.getElementById('bs_lon').value = data.bs_coords[1];
            document.getElementById('ip_address').value = data.server;
            document.getElementById('speed').value = data.bandwidth;
            document.getElementById('duration').value = data.duration;
            document.getElementById('num_packets').value = data.n_packets;
            document.getElementById('packet_size').value = data.packet_size;
            document.getElementById('ant_height').value = data.ant_height;
            document.getElementById('bs_height').value = data.bs_height;
            if (data.freq) {
                document.getElementById('freq').value = data.freq;
            }
            if (data.fresnel_image) {
                document.getElementById('fresnel_test').hidden = false;
            }
            if (data.p_loss) {
                document.getElementById('ping_test').hidden = false;
            }
            if (data.upload) {
                document.getElementById('speed_test').hidden = false;
            }
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}