function getSignalParams() {
    var request = new XMLHttpRequest();

    document.getElementById('loading_bs_list').hidden = true;
    document.getElementById('bs_list_title').style.display = 'none';

    request.open('GET', 'api/v1.0/signal', true);
    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('loading_modem').hidden = true;
            document.getElementById('form_body').style.display = 'block';

            if (data.error) {
                document.getElementById('modem_error').style.display = 'block'
                document.getElementById('asu').value = 0;
                document.getElementById('signal-metrics').innerHTML = 'Отсутсвует!'
                document.getElementById('signal-metrics').style.color ='red'
            }
            else  {
                document.getElementById('asu').value = data.asu;
                document.getElementById('signal-metrics').innerHTML =
                `Оператор: ${data.operator_name} MCC, MNC: (${data.operator_code})<br>
Идентификатор БС: ${data.cellid}<br>
Тип сети: ${data.net_type}<br>
Уровень сигнала: ${data.rssi} dBm`
            }

        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}