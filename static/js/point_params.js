function getPointParams() {
    var request = new XMLHttpRequest();

    request.open('GET', 'api/v1.0/point', true);

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            let data = JSON.parse(this.response)
            document.getElementById('point_name').value = data.point_name;
            document.getElementById('point_lat').value = data.point_lat;
            document.getElementById('point_lon').value = data.point_lon;
        } else {
            alert('Сервер вернул ошибку!')
        }
    };

    request.onerror = function() {
        alert('Нет соединения с устройством!')
    };
    request.send();
}

function postMainParams() {
    var request = new XMLHttpRequest();
    request.open('POST', 'api/v1.0/point', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    let body = "point_name=" + document.getElementById('point_name').value +
        "&point_lat=" + document.getElementById('point_lat').value +
        "&point_lon=" + document.getElementById('point_lon').value
    request.send(body);
    alert('Параметры успешно сохранены!')
}