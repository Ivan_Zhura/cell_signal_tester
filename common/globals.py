"""
Global functions and variables
"""
import os


def object_coordinates_not_exist(results_dict):
    return 'rec_coords' not in results_dict


def bs_coordinates_not_exist(results_dict):
    return 'bs_coords' not in results_dict


def save_results(result_dict, **kwargs):
    result_dict.update(kwargs)


DATABASE_CONN = None

RESULTS = {
    'bs_height': 75,
    'ant_height': 3,
    'server': 'ya.ru',
    'n_packets': 15,
    'packet_size': 64,
    'bandwidth': 0,
    'duration': 7,
    'point_coords': (None, None),
    'bs_coords': (None, None),
}

ANT_IMAGES_PATH = '../static/images/antennas'

ANTENNAS = {
    'АКМ-234': {
        'gain': {
            850: 7,
            900: 12,
            1800: 14,
            2700: 14
        },
        'net_types': ('GPRS', 'UMTS', 'LTE'),
        'focus': False,
        'image': os.path.join(ANT_IMAGES_PATH, 'АКМ-234.png')
    },
}
