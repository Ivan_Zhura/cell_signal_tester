from common.modems.modem_interface import Modem
from common.config import IRZ_USERNAME, IRZ_PASSWORD


class IrzR2(Modem):

    def __init__(self):
        pass

    def establish_connection(self):
        pass

    def get_rssi(self):
        """
        Get signal strength in dBm
        :return: int, RSSI
        """
        pass

    def get_cell_id(self):
        """
        Get Cell ID number
        :param client: huawei API Huawei API client object
        :return: str, cell ID number
        """
        pass

    def get_current_frequency(self):
        pass

    def get_current_net_type(self):
        pass

    def get_network_mode(self):
        pass

    def get_current_mobile_operator(self):
        """
        Get current mobile operator name and code
        :param client: huawei API Huawei API client object
        :return: (str, str), MCC and MNC
        """
        pass

    def get_lac(self):
        """
        Get current LAC (Location Area Code)
        :return: int, LAC
        """
        pass

    def set_network_type(self, net_type):
        """
        :param client: Huawei API client object
        :param net_type: network type: str
            - auto - all network types
            - 2g - 2G only mode
            - 3g - 3G only mode
            - lte - LTE only mode
        :param networkband: str

        """
        pass

    def set_apn(self, apn, apn_username, apn_password):
        pass