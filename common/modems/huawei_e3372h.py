from time import sleep
import requests
from requests.exceptions import ConnectionError as ModemConnectionError
from xml.etree import ElementTree
from huawei_lte_api.Client import Client
from huawei_lte_api.AuthorizedConnection import AuthorizedConnection
from huawei_lte_api.exceptions import ResponseErrorException
from common.modems.modem_interface import Modem
from common.config import HUAWEI_USERNAME, HUAWEI_PASSWORD


class HuaweiE3372(Modem):

    def __init__(self):
        self.connection = None
        self.client = None

    def __str__(self):
        return 'huawei_e3372h'

    def establish_connection(self):
        try:
            self.connection = AuthorizedConnection(
                f'http://{HUAWEI_USERNAME}:{HUAWEI_PASSWORD}@192.168.8.1/',
                timeout=10.0
            )
            self.client = Client(self.connection)
        except ModemConnectionError:
            self.connection = None
            self.client = None

    def get_rssi(self):
        """
        Get signal strength in dBm
        :return: int, RSSI
        """
        self.establish_connection()
        api_answer = self.client.device.signal()
        rssi = api_answer.get('rssi')
        if rssi is None:
            rssi = -113
        else:
            try:
                rssi = int(rssi[:-3])
            except ValueError:
                rssi = int(rssi[2:-3])
        return rssi

    def get_cell_id(self):
        """
        Get Cell ID number
        :return: str, cell ID number
        """
        self.establish_connection()
        api_answer = self.client.device.signal()
        cell_id = api_answer.get('cell_id')
        if cell_id is None:
            cell_id = 'Нет сети!'
        return cell_id

    def get_current_frequency(self):
        """"
        - 80 - GSM1800
        - 300 - GSM900
        - 80000 - GSM850
        - 200000 - GSM1900
        - 400000 - UTMS B1(2100)
        - 2000000000000 - UMTS В8(900)
        - 3FFFFFFF - all bands
        """

        frequency_dict = {
            '80': 1800,
            '300': 900,
            '80000': 850,
            '200000': 1900,
            '400000': 2100,
            '2000000000000': 900,
            '3FFFFFFF': 'Частота не определена!'
        }

        self.establish_connection()
        try:
            raw_data = self.client.net.net_mode()
        except AttributeError:
            raw_data = {
                'NetworkMode': None,
                'NetworkBand': '3FFFFFFF'
            }
        _current_net_mode = raw_data['NetworkMode']
        current_band = raw_data['NetworkBand']
        if _current_net_mode == '03' or self.get_current_net_type() == '4G':
            url = 'http://192.168.8.1/config/deviceinformation/add_param.xml'
            try:
                response = requests.get(url, timeout=10)
                tree = ElementTree.fromstring(response.text)
                return int(tree.find('freq1').text) // 10
            except (requests.exceptions.ConnectTimeout, TypeError):
                return 'Частота не определена!'
        return frequency_dict[current_band]

    def get_current_net_type(self):
        """
        Supported Bands:
        - UMTS B1(2100)
        - UMTSB8(900)
        - GSM850
        - GSM900
        - GSM1800
        - GSM1900
        """
        net_type_dict = {
            'GSM': '2G',
            'WCDMA': '3G',
            'LTE': '4G',
            'NO SERVICE': None
        }
        self.establish_connection()
        current_net_type = self.client.device.information()['workmode']
        return net_type_dict[current_net_type]

    def get_network_mode(self):
        self.establish_connection()
        net_mode = self.client.net.net_mode()['NetworkMode']
        if net_mode == '00':
            return 'auto'
        elif net_mode == '01':
            return '2g'
        elif net_mode == '02':
            return '3g'
        elif net_mode == '03':
            return 'lte'

    def get_current_mobile_operator(self):
        """
        Get current mobile operator name and code
        :return: (str, str), MCC and MNC
        """
        try:
            api_answer = self.client.net.current_plmn()
            operator_name = api_answer.get('ShortName')
            operator_code = api_answer.get('Numeric')
        except ResponseErrorException:
            operator_name = None
            operator_code = None
        return operator_name, operator_code

    def get_lac(self):
        """
        Get current LAC (Location Area Code)
        :return: int, LAC
        """
        url = 'http://192.168.8.1/config/deviceinformation/add_param.xml'
        try:
            response = requests.get(url, timeout=10)
            tree = ElementTree.fromstring(response.text)
            return int(tree.find('lac').text, 16)
        except requests.exceptions.ConnectTimeout:
            return None

    def set_network_type(self, net_type):
        """
        :param net_type: network type: str
            - auto - all network types
            - 2g - 2G only mode
            - 3g - 3G only mode
            - lte - LTE only mode
        :param networkband: str
            - 80 - GSM1800
            - 300 - GSM900
            - 80000 - GSM850
            - 200000 - GSM1900
            - 400000 - UTMS B1(2100)
            - 2000000000000 - UMTS - В8(900)
            - 3FFFFFFF - all bands
            - 1 - BC0A
            - 2 - BC0B
            - 4 - BC1
            - 8 - BC2
            - 10 - BC3
            - 20 - BC4
            - 40 - BC5
            - 400 - BC6
            - 800 - BC7
            - 1000 - BC8
            - 2000 - BC9
            - 4000 - BC10
            - 8000 - BC11
            - 10 000 000 - BC12
            - 20000000 - ВС13
            - 80000000 - BC14
        """
        net_types_dict = {
            'auto': '00',
            '2g': '01',
            '3g': '02',
            'lte': '03'
        }
        bands = {
            '80': 1800,
            '300': 900,
            '80000': 850,
            '200000': 1900,
            '400000': 2100,
            '2000000000000': 900
        }

        self.establish_connection()
        try:
            if net_type == 'auto':
                self.client.net.set_net_mode(
                    networkmode=net_types_dict[net_type],
                    networkband='3FFFFFFF',
                    lteband='7FFFFFFFFFFFFFFF'
                )
                sleep(5)
                current_net_type = modem.get_current_net_type()
                if current_net_type == '2g':
                    best_band = self.find_the_best_band(net_types_dict[current_net_type], tuple(bands.keys())[:-2])
                    self.client.net.set_net_mode(
                        networkmode=net_types_dict[current_net_type],
                        networkband=best_band,
                        lteband='7FFFFFFFFFFFFFFF'
                    )
                elif current_net_type == '3g':
                    best_band = self.find_the_best_band(net_types_dict[current_net_type], tuple(bands.keys())[-2:])
                    self.client.net.set_net_mode(
                        networkmode=net_types_dict[current_net_type],
                        networkband=best_band,
                        lteband='7FFFFFFFFFFFFFFF'
                    )
                else:
                    # LTE frequencies can be obtained from the API of the modem
                    self.client.net.set_net_mode(
                        networkmode=net_types_dict[net_type],
                        networkband='3FFFFFFF',
                        lteband='7FFFFFFFFFFFFFFF'
                    )
            elif net_type == '2g':
                best_band = self.find_the_best_band(net_types_dict[net_type], tuple(bands.keys())[:-2])
                self.client.net.set_net_mode(
                    networkmode=net_types_dict[net_type],
                    networkband=best_band,
                    lteband='7FFFFFFFFFFFFFFF'
                )
            elif net_type == '3g':
                best_band = self.find_the_best_band(net_types_dict[net_type], tuple(bands.keys())[-2:])
                self.client.net.set_net_mode(
                    networkmode=net_types_dict[net_type],
                    networkband=best_band,
                    lteband='7FFFFFFFFFFFFFFF'
                )

            else:
                self.client.net.set_net_mode(
                    networkmode=net_types_dict[net_type],
                    networkband='3FFFFFFF',
                    lteband='7FFFFFFFFFFFFFFF'
                )
                sleep(5)
            sleep(5)
        except AttributeError:
            print('The modem is not working!')

    def set_apn(self, apn, apn_username, apn_password):
        pass

    def find_the_best_band(self, net_type, bands):
        self.establish_connection()
        signal_results = {}
        for band in bands:
            self.client.net.set_net_mode(
                networkmode=net_type,
                networkband=band,
                lteband='7FFFFFFFFFFFFFFF'
            )
            sleep(7)
            rssi = self.get_rssi()
            signal_results[rssi] = band
        if max(signal_results) != -113:
            return signal_results[max(signal_results)]
        else:
            self.client.net.set_net_mode(
                networkmode='00',
                networkband='3FFFFFFF',
                lteband='7FFFFFFFFFFFFFFF'
            )
            return 'The selected network type is not available at this point'


if __name__ == '__main__':
    # '80': 1800,
    # '300': 900,
    # '80000': 850,
    # '200000': 1900,
    # '400000': 2100,
    # '2000000000000': 900,
    # '3FFFFFFF': 'Частота не определена!'
    modem = HuaweiE3372()
    print('Auto:')
    modem.set_network_type('auto')
    print(modem.get_current_frequency())
    print('Было:', modem.get_current_net_type())
    print('__________________________')
    print('2G:')
    modem.set_network_type('2g')
    print(modem.get_current_frequency())
    print('__________________________')
    print('3G:')
    modem.set_network_type('3g')
    print(modem.get_current_frequency())
    print('__________________________')
    print('4G:')
    modem.set_network_type('lte')
    print(modem.get_current_frequency())
    # modem.set_network_type('lte')
    # print(modem.get_current_frequency())
    # print(modem.get_current_net_type())
