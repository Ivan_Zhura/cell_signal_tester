from common.modems.huawei_e3372h import HuaweiE3372

MODEM_IP_ADDRESSES = [
    '192.168.8.1',   # Huawei E3372H
    '192.168.88.1',  # Teleofis
    '192.168.1.1'    # IRZ
]

modem = HuaweiE3372()
