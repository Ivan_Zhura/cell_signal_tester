from abc import ABCMeta, abstractmethod


class Modem:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    def get_rssi(self):
        pass

    @abstractmethod
    def get_cell_id(self):
        pass

    @abstractmethod
    def get_current_frequency(self):
        pass

    @abstractmethod
    def get_current_net_type(self):
        pass

    @abstractmethod
    def get_network_mode(self):
        pass

    @abstractmethod
    def get_current_mobile_operator(self):
        pass

    @abstractmethod
    def get_lac(self):
        pass

    @abstractmethod
    def set_network_type(self, net_type):
        pass

    @abstractmethod
    def set_apn(self, apn, apn_username, apn_password):
        pass

    @staticmethod
    def convert_rssi_to_asu(rssi, net_type):
        if net_type == '2G':
            asu = (rssi + 113) / 2
        elif net_type == '3G':
            asu = rssi + 116
        elif net_type == '4G':
            asu = 1
        else:
            asu = 0
        return asu

    def measure_signal_params(self):
        rssi = self.get_rssi()
        cellid = self.get_cell_id()
        freq = self.get_current_frequency()
        net_type = self.get_current_net_type()
        operator_name, operator_code = self.get_current_mobile_operator()
        asu = self.convert_rssi_to_asu(rssi, net_type)
        return rssi, cellid, freq, net_type, operator_name, operator_code, asu

    def measure_bs_params(self):
        cellid = self.get_cell_id()
        net_type = self.get_current_net_type()
        lac = self.get_lac()
        _operator_name, operator_code = self.get_current_mobile_operator()
        return cellid, lac, net_type, operator_code
