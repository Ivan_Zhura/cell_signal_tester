"""Speed tests functions module"""
import re
import subprocess
import numpy as np
from scipy import stats


def convert_speed_results_to_kbps(speed_list):
    """
    function to convert all speed test results to Kbps
    :param speed_list: list of tuples containing pairs (speed, unit)
    :return: list of measurements in Kbps
    """
    result = []
    for group in speed_list:
        if group[1] == 'Mbits/sec':
            result.append(float(group[0]) * 1000)
        elif group[1] == 'bits/sec':
            result.append(float(group[0]) / 1000)
        else:
            result.append(float(group[0]))
    return result


def check_speed_results(speed_test_function):
    def wrapper(server, bandwidth=None, duration='100', file=None):
        results_dict = {}
        bandwidth_regex = re.compile(r'(\d*.\d*) (.?bits/sec)')
        upload_string, download_string = speed_test_function(server, bandwidth, duration, file)
        upload_list = re.findall(bandwidth_regex, upload_string)[:-2]
        download_list = re.findall(bandwidth_regex, download_string)[:-2]
        upload_data = convert_speed_results_to_kbps(upload_list)
        download_data = convert_speed_results_to_kbps(download_list)
        results_dict['speed_result_str'] = 'На сервер:\n' + upload_string + '\n\n' +\
                                           'C сервера:\n' + download_string
        results_dict['upload_data'] = upload_data
        results_dict['download_data'] = download_data
        try:
            results_dict['upload_mode'] = round(stats.mode(upload_data)[0][0], 2)
            results_dict['download_mode'] = round(stats.mode(download_data)[0][0], 2)
        except IndexError:
            results_dict['upload_mode'] = 0
            results_dict['download_mode'] = 0
        if upload_string == 'Нет соединения с сервером!' and download_string == 'Нет соединения с сервером!':
            results_dict['upload'] = 0
            results_dict['download'] = 0
        else:
            results_dict['upload'] = round(np.array(upload_data).mean(), 2)
            results_dict['download'] = round(np.array(download_data).mean(), 2)
        return results_dict
    return wrapper


@check_speed_results
def iperf3_speed_test(server, bandwidth, duration, file):
    try:
        base_args = ['iperf3', '-c', server, '-t', duration]
        if bandwidth:
            base_args.extend(['-b', bandwidth])
        if file:
            base_args.extend(['-f', file])

        upload = subprocess.check_output(base_args).decode('utf-8')
        # reverse iperf3 test:
        base_args.append('-R')
        download = subprocess.check_output(base_args).decode('utf-8')
        return upload, download
    except subprocess.CalledProcessError:
        return 'Нет соединения с сервером!', 'Нет соединения с сервером!'


# def speedtest_speed_test(threads=None):
#     import speedtest
#     results = {}
#     for i in range(3):
#         s = speedtest.Speedtest()
#         s.get_best_server()
#         s.download(threads=threads)
#         s.upload(threads=threads)
#         results_dict = s.results.dict()
#         results[i] = {
#             'server': results_dict['server']['host'],
#             'upload': results_dict['upload'],
#             'download': results_dict['download']}
#     return results


if __name__ == '__main__':
    from pprint import pprint
    server_ip = input('Enter the ip-address of the iperf3 server: ')
    print('Base:')
    pprint(iperf3_speed_test(server_ip))
    print('With Bandwidth:')
    pprint(iperf3_speed_test(server_ip, bandwidth='64M', duration='10'))
    print('File:')
    pprint(iperf3_speed_test(server_ip, file='monitoring.py', duration='10'))
    print('Bandwidth and file:')
    pprint(iperf3_speed_test(server_ip, bandwidth='64M', file='monitoring.py', duration='10'))

