from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    IntegerField,
    FloatField,
    PasswordField,
    TextAreaField,
    RadioField
)
from wtforms.validators import DataRequired, Optional
from common.form_validators import (
    URLValidator,
    AuthValidator
)


CSS = 'form-control'
CHECK_CSS_CLASS = 'form-check-input mx-2'


class LoginForm(FlaskForm):
    """Login form"""
    username = StringField(
        validators=[DataRequired('Поле должно быть заполнено!'), AuthValidator()],
        render_kw={'class': CSS, 'placeholder': 'Логин'}
    )
    password = PasswordField(
        validators=[DataRequired('Поле должно быть заполнено!'), AuthValidator()],
        render_kw={'class': CSS, 'placeholder': 'Пароль'}
    )


class PointForm(FlaskForm):
    point_name = StringField(
        validators=[DataRequired()],
        render_kw={'class': CSS, 'placeholder': 'House'}
    )
    point_lat = FloatField(
        validators=[DataRequired('The coordinate must be a decimal number!')],
        render_kw={'class': CSS, 'placeholder': '53.035'}
    )
    point_lon = FloatField(
        validators=[DataRequired('The coordinate must be a decimal number!')],
        render_kw={'class': CSS, 'placeholder': '33.212'}
    )


class PingTestForm(FlaskForm):
    ping_results = TextAreaField(render_kw={'class': CSS, 'rows': 5})


class SpeedTestForm(FlaskForm):
    speed_results = TextAreaField(render_kw={'class': CSS, 'rows': 5})


class TestServerForm(FlaskForm):
    ip_address = StringField(
        validators=[DataRequired(), URLValidator()],
        render_kw={'class': CSS, 'placeholder': '10.101.10.1'},
    )
    # for the bandwidth test:
    speed = IntegerField(
        validators=[Optional()],
        render_kw={'class': CSS, 'placeholder': '64'},
    )
    duration = IntegerField(
        validators=[DataRequired('The number of tests must be a number!')],
        render_kw={'class': CSS, 'placeholder': '10'},
    )
    # for the ping test:
    num_packets = IntegerField(
        validators=[DataRequired('The number of packets must be a number!')],
        render_kw={'class': CSS, 'placeholder': '128'},
    )
    packet_size = IntegerField(
        validators=[DataRequired('Package size must be a number!')],
        render_kw={'class': CSS, 'placeholder': '64'},
    )


class ModemSettingsForm(FlaskForm):
    net_type = RadioField(
        choices=[
            ('auto', 'Авто'),
            ('2g', 'Только 2G'),
            ('3g', 'Только 3G'),
            ('lte', 'Только LTE')
        ],
        render_kw={'class': CHECK_CSS_CLASS}
    )


class SignalParamsForm(FlaskForm):
    asu = FloatField(
        validators=[DataRequired('Signal strength must be a number!')],
        render_kw={'class': CSS, 'placeholder': '25'},
    )
    bs_choice = RadioField(
        choices=[],
        render_kw={'class': CHECK_CSS_CLASS}
    )
    bs_lat = FloatField(
        validators=[DataRequired('The coordinate must be a decimal number!')],
        render_kw={'class': CSS, 'placeholder': '53.14'},
    )
    bs_lon = FloatField(
        validators=[DataRequired('The coordinate must be a decimal number!')],
        render_kw={'class': CSS, 'placeholder': '33.345'},
    )


class FresnelForm(FlaskForm):
    freq = IntegerField(
        validators=[DataRequired('Frequency must be a number!')],
        render_kw={'class': CSS, 'placeholder': '900'},
    )
    ant_height = FloatField(
        validators=[DataRequired('The antenna installation height must be a number!')],
        render_kw={'class': CSS, 'placeholder': '3.5'},
    )
    bs_height = FloatField(
        validators=[DataRequired('Base station height must be a number!')],
        render_kw={'class': CSS, 'placeholder': '75'},
    )
