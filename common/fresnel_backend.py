"""Модуль для извлечения данных по высотам. Используются данные SRTM-3:
https://ru.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission.
Класс GeoProfiler инициализируется координатами объекта и БС и названием континента.
Метод, используемый для извлечения данных SRTM - GeoProfiler.make_profile(). Возвращает высоту
над уровнем моря для каждой точки профиля рельефа. Доступ к значению дистанции
м/у объектом и БС осуществляется через атрибут GeoProfiler.dist (значение в км)."""
import os
import numpy as np


class GeoProfiler:
    """Объект для извлечения данных SRTM-3"""
    SRTM_DATA_PATH = os.path.join(os.path.dirname(__file__), 'data', 'srtm_data')
    SAMPLES = 1201  # данные SRTM3 имеют размер 1201x1201:

    def __init__(self, **kwargs):
        """
        Метод для инициализации экземпляра класса GeoProfiler.

        continent='Eurasia', object_coords=point_coords, bs_coords=bs_coords
        :param continent: str. Наименование континента;
        :param bs_coords: tuple. Список или кортеж, содержащий широту и долготу точки БС
        :param object_coords: tuple. Список или кортеж, содержащий широту и долготу объекта.
        """
        self.continent = kwargs.get('continent')
        self.object_coords = kwargs.get('object_coords')
        self.bs_coords = kwargs.get('bs_coords')
        self.coords = []
        self.file_names = []
        self.dist = self.calc_dist()

    def calc_dist(self):
        """
        Метод для расчета дистанции в километрах между двумя координатами.
        :return: float. дистанции в километрах между двумя координатами.
        """
        earth_radius = 6371
        # широты точек а и b
        lat_a = np.radians(self.object_coords[0])
        lat_b = np.radians(self.bs_coords[0])
        # долготы точек а и b
        lon_a = np.radians(self.object_coords[1])
        lon_b = np.radians(self.bs_coords[1])
        # расстояние между точками a и b
        dist = np.arccos(np.sin(lat_a) * np.sin(lat_b) + np.cos(lat_a) *
                         np.cos(lat_b) * np.cos(lon_a - lon_b)) * earth_radius
        return dist

    def make_profile(self):
        """
        Метод для расчета высоты над уровнем моря (в метрах) для точек,
        расположенных между двумя координатами с шагом по умолчанию 90 м.

        :return: list. Список со значениями высоты над уровнем моря для множества
        точек, расположенных на пути между двумя координатами
        """
        try:
            self.coords.append(self.object_coords)
            self.file_names.append(self.get_file_name(*self.object_coords))
            # дистанция, пройденная от начальной точки:
            covered_dist = 0.09
            while covered_dist < self.dist:
                lat_x = self.object_coords[0] + (self.bs_coords[0]
                                                 - self.object_coords[0]) * covered_dist / self.dist
                lon_x = self.object_coords[1] + (self.bs_coords[1]
                                                 - self.object_coords[1]) * covered_dist / self.dist
                self.coords.append((lat_x, lon_x))
                self.file_names.append(self.get_file_name(lat_x, lon_x))
                covered_dist += 0.09  # шаг 90 м (разрешение STRM-3)
            self.coords.append(self.bs_coords)
            self.file_names.append(self.get_file_name(*self.bs_coords))
        except KeyError:
            print('Похоже, что по заданным координатам нет данных SRTM. Проведите проверку в Google Earth')
        return self.read_elevation_from_file()

    @staticmethod
    def get_file_name(lat, lon):
        """
        Метод, который возвращает имя hgt файла, например N27E086.hgt,
        объединенное с путем до директории, где хранятся hgt файлы.

        :param lat: float. широта.
        :param lon: float. долгота.
        :return: str. Путь до файла hgt.
        """
        if lat >= 0:
            ns = 'N'
        elif lat < 0:
            ns = 'S'
        if lon >= 0:
            ew = 'E'
        elif lon < 0:
            ew = 'W'
        hgt_file = "%(ns)s%(lat)02d%(ew)s%(lon)03d.hgt" % \
                   {'lat': abs(lat), 'lon': abs(lon), 'ns': ns, 'ew': ew}
        return hgt_file

    def read_elevation_from_file(self):
        """
        Метод для чтения файла hgt и извлечения из него необходимой информации.

        :return: list. массив высот над уровнем моря над уровнем моря.
        """
        heights = []  # список для хранения значений высоты над уровнем моря
        if len(set(self.file_names)) == 1:
            hgt_file = self.file_names[0]
            hgt_file_path = os.path.join(self.SRTM_DATA_PATH, f'{self.continent}', f'{hgt_file}')
            with open(hgt_file_path, 'rb') as hgt_data:
                # Читаем файл SRTM и сохраняем его в np.array:
                elevations = np.fromfile(
                    hgt_data,
                    np.dtype('>i2'),
                    self.SAMPLES ** 2
                ).reshape((self.SAMPLES, self.SAMPLES))
                for coord in self.coords:
                    lat_row = int(round((coord[0] - int(coord[0])) * (self.SAMPLES - 1), 0))
                    lon_row = int(round((coord[1] - int(coord[1])) * (self.SAMPLES - 1), 0))
                    heights.append(elevations[self.SAMPLES - 1 - lat_row, lon_row].astype(int))
        else:
            file_uniques = set(self.file_names)
            file_indexes = [self.file_names.index(i) for i in file_uniques]
            file_dict = sorted(dict(zip(file_indexes, file_uniques)).items())
            prev_stop_index = 0
            serial_num = 1
            for index, hgt_file in file_dict:
                try:
                    stop_index = file_dict[serial_num][0]
                except IndexError:
                    stop_index = len(self.coords)
                hgt_file_path = os.path.join(self.SRTM_DATA_PATH, f'{self.continent}', f'{hgt_file}')
                with open(hgt_file_path, 'rb') as hgt_data:
                    # Читаем файл SRTM и сохраняем его в np.array:
                    elevations = np.fromfile(
                        hgt_data,
                        np.dtype('>i2'),
                        self.SAMPLES ** 2
                    ).reshape((self.SAMPLES, self.SAMPLES))
                    for coord in self.coords[prev_stop_index:stop_index]:
                        lat_row = int(round((coord[0] - int(coord[0])) * (self.SAMPLES - 1), 0))
                        lon_row = int(round((coord[1] - int(coord[1])) * (self.SAMPLES - 1), 0))
                        heights.append(elevations[self.SAMPLES - 1 - lat_row, lon_row].astype(int))
                prev_stop_index = stop_index
                serial_num += 1
        return np.array(heights)


# def prepare_plotting_data(plot_function):
#     """Декоратор, подготавливающий данные для построения I зоны Френеля"""
#     def wrapper(rec_coords, bs_coords, freq, bs_height, rec_ant_height, results_dict):
#         gp = GeoProfiler(
#             continent='Eurasia',
#             object_coords=rec_coords,
#             bs_coords=bs_coords
#         )
#         meters_dist = gp.dist * 1000
#         try:
#             heights = gp.make_profile()
#         except FileNotFoundError:
#             heights = None
#         heights.insert(0, heights[0]+rec_ant_height)
#         heights.append(heights[-1]+bs_height)
#         plot_function(heights, freq, meters_dist, results_dict)
#     return wrapper


# @prepare_plotting_data
def plot_fresnel_zone(point_coords, bs_coords, freq, bs_height, ant_height, results_dict):
    """Функция для построения I зоны Френеля"""
    gp = GeoProfiler(
        continent='Eurasia',
        object_coords=point_coords,
        bs_coords=bs_coords
    )
    meters_dist = gp.dist * 1000
    try:
        heights = gp.make_profile()
    except FileNotFoundError:
        heights = None
    heights = np.insert(heights, 0, heights[0] + ant_height)
    heights = np.append(heights, heights[-1] + bs_height)
    # rcParams['figure.figsize'] = 8, 4
    # fig, subplot = plt.subplots(dpi=300)
    # массив со значениями дистанции:
    x_earth = np.linspace(0, meters_dist, num=len(heights)-2)
    x_earth = np.insert(x_earth, 0, x_earth[0])
    x_earth = np.append(x_earth, x_earth[-1])
    # график функции:
    # subplot.plot(x_earth, heights, color='green', label='Рельеф')
    # y_zero = [min(heights) - 5 for i in range(len(heights))]
    # subplot.fill_between(x_earth, y_zero, heights, color='darkseagreen')
    # plt.title(name)
    # plt.ylabel('Высоты над уровнем моря, м')
    # plt.xlabel('Удаленность от места установки, м')
    # координаты реклоузера и БС:
    point = np.array((0, heights[0]))
    bs = np.array((x_earth[-1], heights[-1]))
    # построение линии прямой видимости:
    # subplot.plot(
    #     (recloser[0], bs[0]),
    #     (recloser[1], bs[1]),
    #     ls='--',
    #     color=(1, 0, 0, 1),
    #     label='Линия прямой видимости'
    # )
    # координаты центра 1-й зоны Френеля:
    x0 = (point[0] + bs[0]) / 2
    y0 = (point[1] + bs[1]) / 2
    # угол наклона эллипса:
    phi = np.arctan2((bs[1] - point[1]), (bs[0] - point[0]))
    # большая и малая полуоси эллипса:
    a = np.sqrt((bs[0] - point[0]) ** 2 + (bs[1] - point[1]) ** 2) / 2
    b = 17.32 * np.sqrt((a * 2 / 1000) / (4 * freq / 1000))
    b_60 = b * 0.6
    # Построение 1-ой зоны Френеля:
    resolution = 480
    t = np.linspace(0, 2 * np.pi, resolution)
    x = x0 + a * np.cos(t) * np.cos(phi) - b * np.sin(t) * np.sin(phi)
    y = y0 + a * np.cos(t) * np.sin(phi) + b * np.sin(t) * np.cos(phi)
    # Построение 60% 1-ой зоны Френеля:
    x_60 = x0 + a * np.cos(t) * np.cos(phi) - b_60 * np.sin(t) * np.sin(phi)
    y_60 = y0 + a * np.cos(t) * np.sin(phi) + b_60 * np.sin(t) * np.cos(phi)
    # subplot.plot(x, y, label='I-я зона Френеля')
    # subplot.plot(x_60, y_60, label='60% I-й зоны Френеля')
    # Отображение фокусов эллипса (приемник/передатчик)
    # subplot.plot(recloser[0], recloser[1], 'bo')
    # subplot.plot(bs[0], bs[1], 'bo')
    # subplot.fill(x, y, alpha=0.5, color='blue')
    # subplot.fill(x_60, y_60, alpha=0.6, color='yellow')
    # subplot.legend(fontsize=12)
    # Проверка зоны Френеля:
    fresnel_60_top_overlap, fresnel_60_bottom_overlap, direct_line_overlap, bad_markers_x, bad_markers_y = \
        check_fresnel(heights, x_earth, x_60, y_60, resolution, bs, point)
    write_results_to_dict(results_dict,
                          fresnel_60_top_overlap,
                          fresnel_60_bottom_overlap,
                          direct_line_overlap,
                          meters_dist,
                          heights)
    return {
        'x_earth': x_earth.tolist(),
        'heights': heights.tolist(),
        'x60': x_60.tolist(),
        'y60': y_60.tolist(),
        'x': x.tolist(),
        'y': y.tolist(),
        'point': point.tolist(),
        'bs': bs.tolist(),
        'bad_markers_x': bad_markers_x.tolist(),
        'bad_markers_y': bad_markers_y.tolist()
    }


def write_results_to_dict(results_dict,
                          fresnel_60_top_overlap,
                          fresnel_60_bottom_overlap,
                          direct_line_overlap,
                          meters_dist,
                          heights):
    """Функция для записи метрик оценки зоны Френеля в словарь результатов"""
    results_dict['fresnel_60_top_overlap'] = fresnel_60_top_overlap
    results_dict['fresnel_60_bottom_overlap'] = fresnel_60_bottom_overlap
    results_dict['direct_line_overlap'] = direct_line_overlap
    results_dict['dist'] = meters_dist / 1000
    try:
        results_dict['point_bs_h'] = heights[0] - heights[-1]
        results_dict['delta_point'] = max(heights[1:-1]) - heights[0]
        results_dict['delta_bs'] = max(heights[1:-1]) - heights[-1]
        results_dict['dist'] = round(meters_dist / 1000, 2)
    except ValueError:
        results_dict['point_bs_h'] = heights[0] - heights[-1]
        results_dict['delta_point'] = (heights[0] + heights[1]) / 2 - heights[0]
        results_dict['delta_bs'] = (heights[0] + heights[1]) / 2 - heights[-1]
        results_dict['dist'] = round(meters_dist / 1000, 2)


def check_fresnel(heights, x_earth, x_60, y_60, resolution, bs, point):
    """
    Функция, проверяющая, перекрыты ли:
    - линия прямой видимости,
    - нижняя граница 60% I зоны Френеля
    - верхняя граница 60% I зоны Френеля
    """
    x_60_1 = x_60[:resolution // 2]
    x_60_2 = x_60[resolution // 2:]
    y_60_1 = y_60[:resolution // 2]
    y_60_2 = y_60[resolution // 2:]

    fresnel_60_top_overlap = False
    fresnel_60_bottom_overlap = False
    direct_line_overlap = False
    # Массив для хранения точек пересечения:
    bad_markers_x = np.array([])
    bad_markers_y = np.array([])
    # Проверка верхней границы:
    for index, x in enumerate(x_earth[::-1]):
        if index == 0:
            continue
        errors = abs(x_60_1 - x)
        ind = list(errors).index(errors.min())
        if heights[::-1][index] - y_60_1[ind] > 0:
            fresnel_60_top_overlap = True
            fresnel_60_bottom_overlap = True
            direct_line_overlap = True
            bad_markers_x = np.append(bad_markers_x, x)
            bad_markers_y = np.append(bad_markers_y, heights[::-1][index])
    # Проверка линии прямой видимости:
    if not direct_line_overlap:
        k = (point[1] - bs[1]) / (point[0] - bs[0])
        b = bs[1] - k * bs[0]
        for index, x in enumerate(x_earth):
            if index == 0:
                continue
            y = k * x + b
            if heights[index] > y:
                fresnel_60_bottom_overlap = True
                direct_line_overlap = True
                bad_markers_x = np.append(bad_markers_x, x)
                bad_markers_y = np.append(bad_markers_y, y)
    # Проверка нижней границы:
    if not fresnel_60_bottom_overlap:
        for index, x in enumerate(x_earth):
            if index == 0:
                continue
            errors = abs(x_60_2 - x)
            ind = list(errors).index(errors.min())
            if heights[index] - y_60_2[ind] > 0:
                fresnel_60_bottom_overlap = True
                bad_markers_x = np.append(bad_markers_x, x)
                bad_markers_y = np.append(bad_markers_y, heights[index])
    return fresnel_60_top_overlap, fresnel_60_bottom_overlap, direct_line_overlap, bad_markers_x, bad_markers_y


# для проверки работы:
if __name__ == '__main__':
    import json
    RESULTS = {
        'name': 'TEST',
        'freq': 900,
        'point_coords': (58.1, 33.2),
        'bs_coords': (57.1, 32.1)

    }

    freq = 900
    bs_height = 75
    ant_height = 3.5

    res = plot_fresnel_zone(
        RESULTS['point_coords'],
        RESULTS['bs_coords'],
        RESULTS['freq'],
        bs_height,
        ant_height,
        RESULTS
    )
    print(res)
    print(json.dumps(res))
    print(RESULTS)
