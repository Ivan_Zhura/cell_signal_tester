from os.path import join as path_join
import json
from datetime import datetime
import psycopg2


# Postgres staff:
POSTGRES_DBNAME = 'cell_signal_tester'
POSTGRES_USER = 'postgres'


def save_to_json(results_dict):
    with open(path_join('common', 'data', f'{results_dict.get("name")}-{datetime.now().timestamp()}.json'), 'w') as json_file:
        json.dump(results_dict, json_file)


def save_to_database(results_dict):
    conn = psycopg2.connect("dbname=cell_signal_tester user=postgres")
    cursor = conn.cursor()

    point_coords = results_dict.get('point_coords')
    try:
        _object_SQL = "INSERT INTO objects (name, point_lat, point_lon) VALUES (%s, %s, %s) RETURNING id"
        cursor.execute(_object_SQL, (results_dict.get('name'), point_coords[0], point_coords[1]))
        object_id = cursor.fetchone()[0]
    except psycopg2.errors.UniqueViolation:
        conn.rollback()
        _object_id_SQL = """SELECT id, name, point_lat, point_lon FROM objects 
        WHERE name=%s AND point_lat=%s AND point_lon=%s"""
        cursor.execute(_object_id_SQL, (results_dict.get('name'), point_coords[0], point_coords[1]))
        object_id = cursor.fetchone()[0]

    # Full trash:
    _test_results_SQL = """INSERT INTO tests (
    object_id, server, error, modem_type, datetime, fresnel_image, bs_coords, dist, bs_height,
    ant_height, fresnel_60_top_overlap, fresnel_60_bottom_overlap, direct_line_overlap, delta_point, delta_bs,
    point_bs_h, n_packets, packet_size, min_ping, max_ping, mean_ping, p_loss, count_max, count_max_percent,
    ping_list, ping_raw_results, bandwidth, duration, download_data, upload_data, download, upload,
    speed_result_str, upload_mode, download_mode, rssi, asu, cellid, lac, freq, signal_test, operator_code,
    operator_name, net_type, net_mode
    ) VALUES (
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
    )"""
    cursor.execute(
        _test_results_SQL,
        (
            object_id, results_dict.get('server'), results_dict.get('error'), results_dict.get('modem_type'),
            datetime.now(), json.dumps(results_dict.get('fresnel_image')), json.dumps(results_dict.get('bs_coords')),
            results_dict.get('dist'), results_dict.get('bs_height'), results_dict.get('ant_height'),
            results_dict.get('fresnel_60_top_overlap'), results_dict.get('fresnel_60_bottom_overlap'),
            results_dict.get('direct_line_overlap'), results_dict.get('delta_point'), results_dict.get('delta_bs'),
            results_dict.get('point_bs_h'), results_dict.get('n_packets'), results_dict.get('packet_size'),
            results_dict.get('min_ping'), results_dict.get('max_ping'), results_dict.get('mean_ping'),
            results_dict.get('p_loss'), results_dict.get('count_max'), results_dict.get('count_max_percent'),
            json.dumps(results_dict.get('ping_list')), results_dict.get('ping_raw_results'),
            results_dict.get('bandwidth'), results_dict.get('duration'), json.dumps(results_dict.get('download_data')),
            json.dumps(results_dict.get('upload_data')), results_dict.get('download'), results_dict.get('upload'),
            results_dict.get('speed_result_str'), results_dict.get('upload_mode'),
            results_dict.get('download_mode'), results_dict.get('rssi'), results_dict.get('asu'),
            results_dict.get('cellid'), results_dict.get('lac'), results_dict.get('freq'),
            results_dict.get('signal_test'), results_dict.get('operator_code'),
            results_dict.get('operator_name'), results_dict.get('net_type'), results_dict.get('net_mode')
        )
    )
    conn.commit()
    cursor.close()
    conn.close()


def get_data_by_param():
    pass
