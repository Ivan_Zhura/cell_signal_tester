from common.config import USERNAME as admin_username
from common.config import PASSWORD as admin_password
from wtforms.validators import ValidationError

import re


class AuthValidator:
    def __call__(self, form, field):
        if field.type == 'StringField':
            if field.data and field.data != admin_username:
                field.errors[:] = []
                raise ValidationError('Wrong username!')
        elif field.type == 'PasswordField':
            if field.data and field.data != admin_password:
                field.errors[:] = []
                raise ValidationError('Wrong password!')


class URLValidator:
    def __call__(self, form, field):
        url_regex = r'^(https?:\/\/)?(.+\.)?.+\..+'
        ip_regex = r'([0-9]{1,3}[\.]){3}[0-9]{1,3}'
        if field.data[0].isdigit():
            if not re.fullmatch(ip_regex, field.data):
                field.errors[:] = []
                raise ValidationError('Wrong IP-address!')
        elif field.data[0].isalpha():
            if not re.fullmatch(url_regex, field.data):
                field.errors[:] = []
                raise ValidationError('Wrong URL!')
