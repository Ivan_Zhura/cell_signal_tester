"""Config file for application"""
# Common part:
USERNAME = 'admin'
PASSWORD = 'admin'
MONITORING_SCRIPT_COMMAND = 'python3'

# Usernames and passwords for modems:
# 1. Huawei E3372h:
HUAWEI_USERNAME = 'admin'
HUAWEI_PASSWORD = 'admin'
# 2. Telofis RTU968:
TELEOFIS_USERNAME = 'root'
TELEOFIS_PASSWORD = 'root'
# 3. IRZ R2:
IRZ_USERNAME = 'root'
IRZ_PASSWORD = 'root'