import re
import subprocess
import numpy as np


def infinity_filter(x):
    if x == np.inf:
        return None
    return x


def check_ping_results(ping_test_function):
    def wrapper(ip, n_packets, pack_size):
        results_dict = {}
        ping_loss_regex = re.compile(r'(\d*?.?\d*)%')
        ping_list_regex = re.compile(r'time=(\d*.\d*)')
        ping_params_regex = re.compile(r'min\/.* = (\d*.\d*)\/(\d*.\d*)\/(\d*.\d*)\/(\d*.\d*)?')
        ping_string = ping_test_function(ip, n_packets, pack_size)
        ping_list = np.array([])
        for line in ping_string.split('\n')[1:-5]:
            try:
                ping_list = np.append(ping_list, float(re.search(ping_list_regex, line).group(1)))
            except AttributeError:
                ping_list = np.append(ping_list, np.inf)
        p_loss = float(re.search(ping_loss_regex, ping_string)[0][:-1])
        stats = re.search(ping_params_regex, ping_string)
        try:
            ping_mean = float(stats[2])
            results_dict['min_ping'] = float(stats[1])
            results_dict['mean_ping'] = ping_mean
            results_dict['max_ping'] = float(stats[3])
            try:
                ping_std = float(stats[4])
            except TypeError:
                ping_std = round(ping_list.std(), 2)
            results_dict['count_max'] = int(sum(ping_list > ping_mean + ping_std))
        except TypeError:
            results_dict['min_ping'] = 120500
            results_dict['mean_ping'] = 120500
            results_dict['max_ping'] = 120500
            results_dict['count_max'] = int(n_packets)
        results_dict['ping_list'] = list(map(infinity_filter, ping_list))
        results_dict['p_loss'] = p_loss
        results_dict['count_max_percent'] = round(results_dict['count_max'] * 100 / int(n_packets), 2)
        results_dict['ping_raw_results'] = ping_string
        return results_dict
    return wrapper


@check_ping_results
def measure_ping(ip, n_packets, pack_size):
    try:
        return subprocess.check_output(['ping', '-c', n_packets, '-s', pack_size, ip]).decode('utf-8')
    except subprocess.CalledProcessError:
        return f'''No connection to the server!
--- {ip} ping statistics ---
{n_packets} packets transmitted, 0 packets received, 100.0% packet loss'''


if __name__ == '__main__':
    from pprint import pprint
    pprint(measure_ping(ip='8.8.8.8', n_packets='15', pack_size='64'))

