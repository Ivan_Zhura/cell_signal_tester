import json
import argparse
from os.path import join as path_join
import iperf3
from time import sleep
from datetime import datetime
from requests.exceptions import ConnectionError
from common.huawei_api import (
    AuthorizedConnection,
    Client as HuaweiClient,
    get_rssi,
    get_cell_id
)
from common.ping_test_api import measure_ping
from common.admin_config import (
    huawei_username,
    huawei_password
)


def signal_monitoring():
    try:
        connection = AuthorizedConnection(
            f'http://{huawei_username}:{huawei_password}@192.168.8.1/',
            timeout=10.0
        )
        client = HuaweiClient(connection)
        rssi = get_rssi(client)
        cellid = get_cell_id(client)
    except ConnectionError:
        rssi = 'Нет связи с модемом!'
        cellid = 'Нет связи с модемом!'
    return {'rssi': rssi, 'cellid': cellid}


def iperf3_monitoring(server, duration=10, bandwidth=None, port=5201):
    client = iperf3.Client()
    client.duration = duration
    client.server_hostname = server
    client.port = port
    if bandwidth:
        client.bandwidth = bandwidth
    test_result = client.run()
    json_result = dict(json.loads(str(test_result)))
    try:
        json_result['sent_Kbps'] = test_result.sent_Mbps * 1000
        json_result['received_Kbps'] = test_result.received_Mbps * 1000
    except AttributeError:
        json_result['sent_Kbps'] = 0
        json_result['received_Kbps'] = 0
    return json_result


def monitoring(server, bandwidth):
    result_list = []
    counter = 0
    while True:
        try:
            result = iperf3_monitoring(server=server, bandwidth=bandwidth)
        except IndexError:
            result = {'sent_Kbps': 0, 'received_Kbps': 0}
        result.update({'signal_params': signal_monitoring()})
        result.update({'ping_params': measure_ping(server, '3', '64')})
        result_list.append(result)
        sleep(10)
        if counter == 9:
            with open(path_join('data', 'monitoring', f'{datetime.now()}.json'), 'w') as file:
                json.dump(result_list, file)
                result_list.clear()
                counter = 0
        counter += 1


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='Monitoring')
        parser.add_argument('--server', type=str, help='IP address')
        parser.add_argument('--bandwidth', type=int, help='Bandwidth, KBps')
        args = parser.parse_args()
        monitoring(server=args.server, bandwidth=args.bandwidth)
    except AttributeError:
        monitoring(server='185.80.129.124', bandwidth=64000)
